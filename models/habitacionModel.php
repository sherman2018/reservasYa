<?php 

class habitacionModel extends Model
{
    public function __construct() {
        parent::__construct();
    }

    
    

    public function crear($id_hotel, $capacidad){   
        $this->_db->prepare("INSERT INTO habitacion (capacidad) VALUES (:capacidad)")
                ->execute(
                        array(
                           
                           ':capacidad' => $capacidad
                        ));
                $id_habitacion = $this->_db->lastInsertId();



        $this->_db->prepare("INSERT INTO habitacion_hotel (id_hotel, id_habitacion) VALUES (:id_hotel, :id_habitacion);")
                ->execute(
                        array(
                           ':id_hotel' => $id_hotel,
                           ':id_habitacion' => $id_habitacion
                        ));

    }

    public function agregarCarrito($estado,$precio_persona,$fechaDesde,$fechaHasta,$id_habitacion,$id_usuario,$cantidad){
        $this->_db->prepare("INSERT INTO reserva_habitacion (estado, precio_abonado, desde, hasta) VALUES (:estado, :precio_abonado, :desde, :hasta);")
            ->execute(
                    array(':estado' => $estado,
                            ':precio_abonado' => $precio_persona,
                            ':desde' => $fechaDesde,
                            ':hasta' => $fechaHasta
                     ));
            $id_reserva_habitacion= $this->_db->lastInsertId();



        $this->_db->prepare("INSERT INTO usuario_reserva_habitacion (id_reserva_habitacion, id_usuario) VALUES (:id_reserva_habitacion, :id_usuario);")
            ->execute(
                    array(
                        ':id_reserva_habitacion' => $id_reserva_habitacion,
                        ':id_usuario' => $id_usuario
                        ));



        $this->_db->prepare("INSERT INTO habitacion_reserva_habitacion (id_reserva_habitacion, id_habitacion) VALUES (:id_reserva_habitacion, :id_habitacion);")
            ->execute(
                    array(
                            ':id_reserva_habitacion' => $id_reserva_habitacion,
                            ':id_habitacion' => $id_habitacion
                            ));

        $adicional= ("SELECT * FROM habitacion WHERE id_habitacion = :id_habitacion");
        $pdoConsulta= $this->_db->prepare($adicional);
        $params= array(":id_habitacion" => $id_habitacion);
        $pdoConsulta->execute($params);
        $result= $pdoConsulta->fetch();
        $capacidad= $result['capacidad'];

    }
/////////seguir
    public function eliminar($id_hotel, $cantidad_a_eliminar, $capacidad){

        
            $stmt = $this->_db->prepare("UPDATE habitacion SET eliminado='1' WHERE capacidad=:capacidad and eliminado='0' and id_habitacion in (select id_habitacion from habitacion_hotel where id_hotel=:id_hotel and eliminado='0') LIMIT $cantidad_a_eliminar");
                $stmt->execute(
                        array(
                           
                           ':id_hotel' => $id_hotel,
                           ':capacidad' => $capacidad,
                           

                        )); 
            
        
           
                
        

    }

    public function consumir($id_reserva_habitacion){

            
            $stmt = $this->_db->prepare("UPDATE reserva_habitacion SET desde='2018-12-8' WHERE id_reserva_habitacion=:id_reserva_habitacion");
                $stmt->execute(
                        array(
                           
                           ':id_reserva_habitacion' => $id_reserva_habitacion                          

                        )); 
            
        
           
                
        

    }

    public function buscarHabitacion($ciudad, $desde, $hasta, $cantidad){

        $desde = new DateTime($desde);
        $desde->modify('+1 second');
        $desde=$desde->format('Y-m-d H:i:s');

        $hasta = new DateTime($hasta);
        $hasta->modify('-1 second');
        $hasta=$hasta->format('Y-m-d H:i:s');





        $sql = "SELECT * FROM habitacion NATURAL JOIN habitacion_hotel NATURAL JOIN hotel NATURAL JOIN hotel_ciudad NATURAL JOIN ciudad WHERE (id_ciudad=:ciudad AND capacidad >= :cantidad) AND (eliminado = 0) AND id_habitacion NOT IN (SELECT distinct(id_habitacion) FROM habitacion_reserva_habitacion NATURAL JOIN reserva_habitacion WHERE (((desde  BETWEEN :desde AND :hasta) OR (hasta  BETWEEN :desde AND :hasta) OR (:desde  BETWEEN desde AND hasta) OR (:hasta  BETWEEN desde AND hasta)) AND (estado <> 'cancelado'))) order by hotel.precio_por_persona"; 


        
        $pdoConsulta = $this->_db->prepare($sql);
           //$params = array(":ciudad" => $ciudad, ":cantidad" => $cantidad, ":desde" => $desde->format('Y-m-d H:i:s'), ":hasta" => $hasta->format('Y-m-d H:i:s'));
           $params = array(":ciudad" => $ciudad, ":cantidad" => $cantidad,":desde" => $desde, ":hasta" => $hasta);
           $pdoConsulta->execute($params);
           
           $result = $pdoConsulta->fetchAll(PDO::FETCH_ASSOC);
            
            return $result;
    }  

}




?>