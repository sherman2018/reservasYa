<?php 

class agenciaModel extends Model
{
    public function __construct() {
        parent::__construct();
    }

    public function crear($nombre, $id_franquicia, $id_ciudad){ 
    	$gsent = $this->_db->prepare('INSERT INTO agencia (nombre) values (?)');
      $gsent->execute(array($nombre));
      $id_agencia=$this->_db->lastInsertId();
      $gsent = $this->_db->prepare('INSERT INTO agencia_ciudad (id_agencia,id_ciudad) values (?,?)');
      $gsent->execute(array($id_agencia, $id_ciudad));
      $gsent = $this->_db->prepare('INSERT INTO agencia_franquicia (id_agencia,id_franquicia) values (?,?)');
      $gsent->execute(array($id_agencia, $id_franquicia));
      return ($id_agencia." - ".$nombre);
    }
    public function getAgencias(){
        $gsent = $this->_db->prepare("SELECT id_agencia, id_ciudad,nombre as agencia , ciudad FROM agencia NATURAL JOIN agencia_ciudad NATURAL JOIN ciudad");
        $gsent->execute();
        return $gsent->fetchAll();
    }

    public function getAutos($id_agencia){
        $gsent = $this->_db->prepare("SELECT count(id_auto) as cantidad, marca, gama, capacidad from auto natural join auto_agencia natural join auto_marca natural join marca natural join  auto_gama natural join gama where (id_agencia=? and eliminado = '0') group by capacidad, marca, gama");
        $gsent->execute(array($id_agencia));
        return $gsent->fetchAll();
    }
}
?>