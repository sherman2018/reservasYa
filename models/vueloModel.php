<?php 

class vueloModel extends Model
{
    public function __construct() {
        parent::__construct();
    }

    public function getVuelos(){
    	//$query = $this->_db->query("select * from vuelo natural join vuelo_aerolinea natural join aerolinea natural join vuelo_desde natural join ciudad natural join ciudad_provincia natural join provincia natural join provincia_pais natural join pais order by precio");
    	//var_dump($query->fetchAll());die;
    	$query = $this->_db->query("SELECT
                 *,ori.ciudad as origenCiudad, dest.ciudad as destinoCiudad,ori.provincia as origenProvincia, 
                 dest.provincia as destinoProvincia,ori.pais as origenPais, dest.pais as destinoPais 
                 FROM
                 (SELECT * FROM vuelo NATURAL JOIN vuelo_desde NATURAL JOIN ciudad NATURAL JOIN vuelo_aerolinea 
                 NATURAL JOIN aerolinea NATURAL JOIN ciudad_provincia NATURAL JOIN provincia NATURAL JOIN provincia_pais 
                 NATURAL JOIN pais) ori
                 INNER JOIN
                 (SELECT * FROM vuelo NATURAL JOIN vuelo_hasta NATURAL JOIN ciudad natural join vuelo_aerolinea 
                 natural join aerolinea natural join ciudad_provincia natural join provincia 
                 natural join provincia_pais natural join pais) dest
                 ON ori.id_vuelo = dest.id_vuelo") ;
      	return ($query->fetchAll());
    }
    


    public function crear($id_aerolinea,$fechaSalida, $fechaLlegada, $id_origen, $id_destino, $cantEconomica, $cantPrimera, $cantBusiness, $precio){   
        $this->_db->prepare("INSERT INTO vuelo (salida, llegada, capacidad_primera, capacidad_business, capacidad_economica, precio) VALUES (:salida, :llegada, :capacidad_primera, :capacidad_business, :capacidad_economica, :precio);")
                ->execute(
                        array(
                           ':salida' => $fechaSalida,
                           ':llegada' => $fechaLlegada,
                           ':capacidad_primera' => $cantPrimera,
                           ':capacidad_economica' => $cantEconomica,
                           ':capacidad_business' => $cantBusiness,
                           ':precio' => $precio
                        ));
                $id_vuelo = $this->_db->lastInsertId();



        $this->_db->prepare("INSERT INTO vuelo_aerolinea (id_vuelo, id_aerolinea) VALUES (:id_vuelo, :id_aerolinea);")
                ->execute(
                        array(
                           ':id_vuelo' => $id_vuelo,
                           ':id_aerolinea' => $id_aerolinea
                        ));

        $this->_db->prepare("INSERT INTO vuelo_desde (id_vuelo, id_ciudad) VALUES (:id_vuelo, :id_ciudad);")
                ->execute(
                        array(
                           ':id_vuelo' => $id_vuelo,
                           ':id_ciudad' => $id_origen
                        ));

         $this->_db->prepare("INSERT INTO vuelo_hasta (id_vuelo, id_ciudad) VALUES (:id_vuelo, :id_ciudad);")
                ->execute(
                        array(
                           ':id_vuelo' => $id_vuelo,
                           ':id_ciudad' => $id_destino
                        ));

    }

    public function buscarVuelos($ciudad_origen, $ciudad_destino, $fechaVuelo, $cantidadEconomica, 
        $cantidadPrimera, $cantidadBusiness){
            
        $sql = "SELECT * ,ciu.ciudad as origenCiudad, ciu2.ciudad as destinoCiudad,prov.provincia as origenProvincia, 
                 prov2.provincia as destinoProvincia,p.pais as origenPais, p2.pais as destinoPais 
    FROM vuelo a 
        JOIN vuelo_desde b
            on a.id_vuelo = b.id_vuelo
        JOIN vuelo_hasta c
            on a.id_vuelo = c.id_vuelo
        JOIN ciudad ciu
            on b.id_ciudad = ciu.id_ciudad
        JOIN ciudad ciu2
            on c.id_ciudad = ciu2.id_ciudad
        JOIN vuelo_aerolinea d
            on a.id_vuelo = d.id_vuelo
        JOIN aerolinea e
            on d.id_aerolinea = e.id_aerolinea
        JOIN ciudad_provincia f
            on f.id_ciudad = ciu.id_ciudad
        JOIN ciudad_provincia g
            on g.id_ciudad = ciu2.id_ciudad
        JOIN provincia prov
            on f.id_provincia = prov.id_provincia
        JOIN provincia prov2
            on g.id_provincia = prov2.id_provincia
        JOIN provincia_pais pp
            on pp.id_provincia = prov.id_provincia
        JOIN provincia_pais pp2
            on pp2.id_provincia = prov2.id_provincia
        JOIN pais p
            on p.id_pais = pp.id_pais
        JOIN pais p2
            on p2.id_pais = pp2.id_pais 
        WHERE b.id_ciudad = :ciudad_origen
            and   c.id_ciudad = :ciudad_destino
            and   DATE(a.salida) = :fecha_vuelo
            and   ((a.capacidad_economica >= :cantidad_economica and :cantidad_economica > 0)
             
            or (a.capacidad_business >= :cantidad_business and :cantidad_business > 0) 
             
            or (a.capacidad_primera >= :cantidad_primera and :cantidad_primera > 0))
            and a.eliminado = 0 ORDER BY a.precio";
        
           $pdoConsulta = $this->_db->prepare($sql);
           $params = array(":ciudad_origen" => $ciudad_origen, ":ciudad_destino" => $ciudad_destino, 
               ":fecha_vuelo" => $fechaVuelo, ":cantidad_economica" => $cantidadEconomica,
               ":cantidad_business" => $cantidadBusiness, ":cantidad_primera" => $cantidadPrimera);

            $pdoConsulta->execute($params);
            
            $result = $pdoConsulta->fetchAll();
            
            return $result;
        }    

        public function buscarVuelosConEscalaTramoUno($ciudad_origen,  $fechaVuelo, $cantidadEconomica, 
        $cantidadPrimera, $cantidadBusiness){
            
        $sql = "SELECT * ,ciu.ciudad as origenCiudad, ciu2.ciudad as destinoCiudad,prov.provincia as origenProvincia, 
                 prov2.provincia as destinoProvincia,p.pais as origenPais, p2.pais as destinoPais 
    FROM vuelo a 
        JOIN vuelo_desde b
            on a.id_vuelo = b.id_vuelo
        JOIN vuelo_hasta c
            on a.id_vuelo = c.id_vuelo
        JOIN ciudad ciu
            on b.id_ciudad = ciu.id_ciudad
        JOIN ciudad ciu2
            on c.id_ciudad = ciu2.id_ciudad
        JOIN vuelo_aerolinea d
            on a.id_vuelo = d.id_vuelo
        JOIN aerolinea e
            on d.id_aerolinea = e.id_aerolinea
        JOIN ciudad_provincia f
            on f.id_ciudad = ciu.id_ciudad
        JOIN ciudad_provincia g
            on g.id_ciudad = ciu2.id_ciudad
        JOIN provincia prov
            on f.id_provincia = prov.id_provincia
        JOIN provincia prov2
            on g.id_provincia = prov2.id_provincia
        JOIN provincia_pais pp
            on pp.id_provincia = prov.id_provincia
        JOIN provincia_pais pp2
            on pp2.id_provincia = prov2.id_provincia
        JOIN pais p
            on p.id_pais = pp.id_pais
        JOIN pais p2
            on p2.id_pais = pp2.id_pais
        WHERE b.id_ciudad = :ciudad_origen
            and   DATE(a.salida) = :fecha_vuelo
            and   ((a.capacidad_economica >= :cantidad_economica and :cantidad_economica > 0)
             
            or (a.capacidad_business >= :cantidad_business and :cantidad_business > 0) 
             
            or (a.capacidad_primera >= :cantidad_primera and :cantidad_primera > 0))
            and a.eliminado = 0";
        
           $pdoConsulta = $this->_db->prepare($sql);
           $params = array(":ciudad_origen" => $ciudad_origen,  
               ":fecha_vuelo" => $fechaVuelo, ":cantidad_economica" => $cantidadEconomica,
               ":cantidad_business" => $cantidadBusiness, ":cantidad_primera" => $cantidadPrimera);

            $pdoConsulta->execute($params);
            
            $result = $pdoConsulta->fetchAll();
            
            return $result;
        }   

        public function buscarVuelosConEscalaTramoDos($ciudad_destino,  $fechaVuelo, $cantidadEconomica, 
        $cantidadPrimera, $cantidadBusiness){
            
        $sql = "SELECT * ,ciu.ciudad as origenCiudad, ciu2.ciudad as destinoCiudad,prov.provincia as origenProvincia, 
                 prov2.provincia as destinoProvincia,p.pais as origenPais, p2.pais as destinoPais 
    FROM vuelo a 
        JOIN vuelo_desde b
            on a.id_vuelo = b.id_vuelo
        JOIN vuelo_hasta c
            on a.id_vuelo = c.id_vuelo
        JOIN ciudad ciu
            on b.id_ciudad = ciu.id_ciudad
        JOIN ciudad ciu2
            on c.id_ciudad = ciu2.id_ciudad
        JOIN vuelo_aerolinea d
            on a.id_vuelo = d.id_vuelo
        JOIN aerolinea e
            on d.id_aerolinea = e.id_aerolinea
        JOIN ciudad_provincia f
            on f.id_ciudad = ciu.id_ciudad
        JOIN ciudad_provincia g
            on g.id_ciudad = ciu2.id_ciudad
        JOIN provincia prov
            on f.id_provincia = prov.id_provincia
        JOIN provincia prov2
            on g.id_provincia = prov2.id_provincia
        JOIN provincia_pais pp
            on pp.id_provincia = prov.id_provincia
        JOIN provincia_pais pp2
            on pp2.id_provincia = prov2.id_provincia
        JOIN pais p
            on p.id_pais = pp.id_pais
        JOIN pais p2
            on p2.id_pais = pp2.id_pais
        WHERE c.id_ciudad = :ciudad_destino
            and   DATE(a.salida) >= :fecha_vuelo
            and   ((a.capacidad_economica >= :cantidad_economica and :cantidad_economica > 0)
             
            or (a.capacidad_business >= :cantidad_business and :cantidad_business > 0) 
             
            or (a.capacidad_primera >= :cantidad_primera and :cantidad_primera > 0))
            and a.eliminado = 0";
        
           $pdoConsulta = $this->_db->prepare($sql);
           $params = array(":ciudad_destino" => $ciudad_destino,  
               ":fecha_vuelo" => $fechaVuelo, ":cantidad_economica" => $cantidadEconomica,
               ":cantidad_business" => $cantidadBusiness, ":cantidad_primera" => $cantidadPrimera);

            $pdoConsulta->execute($params);
            
            $result = $pdoConsulta->fetchAll();
            
            return $result;
        }


        public function agregarCarrito($id_vuelo, $precio_abonado, $cantidad_primera, $cantidad_economica, $cantidad_business, $id_usuario, $estado){
            $this->_db->prepare("INSERT INTO reserva_vuelo (estado, cantidad_economica, cantidadad_primera, cantidad_business, precio_abonado) VALUES (:estado, :cantidad_economica, :cantidadad_primera, :cantidad_business, :precio_abonado);")
                ->execute(
                    array(
                           ':estado' => $estado,
                           ':cantidadad_primera' => $cantidad_primera,
                           ':cantidad_economica' => $cantidad_economica,
                           ':cantidad_business' => $cantidad_business,
                           ':precio_abonado' => $precio_abonado
                           ));

            $id_reserva_vuelo= $this->_db->lastInsertId();

            $this->_db->prepare("INSERT INTO usuario_reserva_vuelo (id_reserva_vuelo, id_usuario) VALUES (:id_reserva_vuelo, :id_usuario);")
            ->execute(
                    array(
                        ':id_reserva_vuelo' => $id_reserva_vuelo,
                        ':id_usuario' => $id_usuario
                        ));

            $this->_db->prepare("INSERT INTO vuelo_reserva_vuelo (id_reserva_vuelo, id_vuelo) VALUES (:id_reserva_vuelo, :id_vuelo);")
            ->execute(
                    array(
                            ':id_reserva_vuelo' => $id_reserva_vuelo,
                            ':id_vuelo' => $id_vuelo
                            ));

        $adicional= ("SELECT * FROM vuelo WHERE id_vuelo = :id_vuelo");
        $pdoConsulta= $this->_db->prepare($adicional);
        $params= array(":id_vuelo" => $id_vuelo);
        $pdoConsulta->execute($params);
        $result= $pdoConsulta->fetch();
        $capacidad_primera= $result['capacidad_primera'];
        $capacidad_business= $result['capacidad_business'];
        $capacidad_economica=$result['capacidad_economica'];

        $this->_db->prepare("UPDATE vuelo SET capacidad_primera= $capacidad_primera - :cantidad_primera, capacidad_business= $capacidad_business - :cantidad_business, capacidad_economica= $capacidad_economica - :cantidad_economica WHERE (id_vuelo=:id_vuelo)")
                ->execute(
                            array(
                            ':cantidad_primera' => $cantidad_primera,
                            ':cantidad_business' => $cantidad_business,
                            ':cantidad_economica' => $cantidad_economica,
                            ':id_vuelo' => $id_vuelo
                            ));

        }   

        
    
}

?>