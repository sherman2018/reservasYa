<?php 

class gamaModel extends Model
{
    public function __construct() {
        parent::__construct();
    }

    public function getGamas(){
        $gsent = $this->_db->prepare("SELECT * FROM gama");
        $gsent->execute();
        return $gsent->fetchAll();
    }
}
?>