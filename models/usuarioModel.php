<?php 

class usuarioModel extends Model
{
    public function __construct() {
        parent::__construct();
    }
    
  
    public function setUsuario($esComercial, $esCliente, $nombre, $apellido, $dni, $nombre_de_usuario, $mail, $contrasena,$pregunta_secreta, $respuesta_secreta)
    {
        $this->_db->prepare("INSERT INTO usuario (nombre, apellido, dni, nombre_de_usuario, mail, contrasena, pregunta_secreta, respuesta_secreta, cliente, comercial) VALUES (:nombre, :apellido, :dni, :nombre_de_usuario, :mail, :contrasena, :pregunta_secreta, :respuesta_secreta, :cliente, :comercial)")
                ->execute(
                        array(
                           ':nombre' => $nombre,
                           ':apellido' => $apellido,
                           ':dni' => $dni,
                           ':nombre_de_usuario' => $nombre_de_usuario,
                           ':mail' => $mail,
                           ':contrasena' => $contrasena,
                           ':pregunta_secreta' => $pregunta_secreta,
                           ':respuesta_secreta' => $respuesta_secreta,
                           ':cliente' => $esCliente,
                           ':comercial' => $esComercial
                         
                        ));
    }
    public function getUsuarioByUsername($username){
      $query = $this->_db->query("select * from usuario where nombre_de_usuario = '".$username."'");
      return ($query->rowCount());
    }

    public function getUsuarioByMail($mail){
      $query = $this->_db->query("select * from usuario where mail = '".$mail."'");
      return ($query->rowCount());
    }

    public function validarUsuarioPorUsername($nombre_de_usuario, $id_usuario){
      $query = $this->_db->query("select * from usuario where id_usuario != $id_usuario and nombre_de_usuario = '".$nombre_de_usuario."'");
      return ($query->rowCount());
    }

    public function validarUsuarioPorMail($mail, $id_usuario){
      $query = $this->_db->query("select * from usuario where id_usuario != $id_usuario and mail = '".$mail."'");
      return ($query->rowCount());
    }

    public function update_Usuario($nombre_de_usuario, $mail, $nombre, $apellido, $dni, $pregunta_secreta, $respuesta_secreta, $id_usuario){
      $this->_db->prepare("UPDATE usuario SET nombre_de_usuario= :nombre_de_usuario, mail= :mail, nombre= :nombre, apellido= :apellido, dni= :dni, pregunta_secreta= :pregunta_secreta, respuesta_secreta= :respuesta_secreta WHERE (id_usuario=:id_usuario)")
            ->execute(
                      array(
                        ':nombre_de_usuario' => $nombre_de_usuario,
                        ':mail' => $mail,
                        ':nombre' => $nombre,
                        ':apellido' => $apellido,
                        ':dni' => $dni,
                        ':pregunta_secreta' => $pregunta_secreta,
                        ':respuesta_secreta' => $respuesta_secreta,
                        ':id_usuario' => $id_usuario
                        ));

    }

    public function autenticarUsuario($username, $contrasena){
      $query = $this->_db->query("select id_usuario,nombre_de_usuario,admin,comercial,cliente from usuario where nombre_de_usuario = '".$username."' and contrasena = '".$contrasena."'");
      $usuario = $query->fetchAll();
      if (isset($usuario[0]) && !is_null($usuario[0]['id_usuario'])){
        return ($usuario);  
      }
      return 0;
    }
    
    public function getUsuario($username){
      $query = $this->_db->query("select * from usuario where nombre_de_usuario = '".$username."'");
      $query->execute();
      $user=$query->fetch();
      return $user;
    }
    public function obtenerVuelos($id_usuario){
/*
      
      $query = $this->_db->query("SELECT
                 *,ori.ciudad as origenCiudad, dest.ciudad as destinoCiudad,ori.provincia as origenProvincia, 
                 dest.provincia as destinoProvincia,ori.pais as origenPais, dest.pais as destinoPais 
                 FROM
                 (SELECT * FROM vuelo NATURAL JOIN vuelo_desde NATURAL JOIN ciudad NATURAL JOIN vuelo_aerolinea 
                 NATURAL JOIN aerolinea NATURAL JOIN ciudad_provincia NATURAL JOIN provincia NATURAL JOIN provincia_pais 
                 NATURAL JOIN pais NATURAL JOIN reserva_vuelo NATURAL JOIN vuelo_reserva_vuelo NATURAL JOIN usuario_reserva_vuelo ) ori
                 INNER JOIN
                 (SELECT * FROM vuelo NATURAL JOIN vuelo_hasta NATURAL JOIN ciudad natural join vuelo_aerolinea 
                 natural join aerolinea natural join ciudad_provincia natural join provincia 
                 natural join provincia_pais natural join pais NATURAL JOIN reserva_vuelo NATURAL JOIN vuelo_reserva_vuelo NATURAL JOIN usuario_reserva_vuelo ) dest
                 ON (ori.id_vuelo = dest.id_vuelo and ori.id_usuario=dest.id_usuario) ") ;
        return ($query->fetchAll());*/
       
$query = $this->_db->query("SELECT 
                 *,ori.ciudad as origenCiudad, dest.ciudad as destinoCiudad,ori.provincia as origenProvincia, 
                 dest.provincia as destinoProvincia,ori.pais as origenPais, dest.pais as destinoPais  from vuelo_reserva_vuelo natural join reserva_vuelo natural join usuario_reserva_vuelo natural join vuelo NATURAL JOIN aerolinea 
                 NATURAL JOIN vuelo_aerolinea tbl 
  INNER JOIN
  (SELECT * FROM vuelo_desde NATURAL JOIN ciudad NATURAL JOIN ciudad_provincia NATURAL JOIN provincia NATURAL JOIN provincia_pais NATURAL JOIN pais) ori

  INNER JOIN
  (SELECT * FROM vuelo_hasta NATURAL JOIN ciudad NATURAL JOIN ciudad_provincia NATURAL JOIN provincia NATURAL JOIN provincia_pais NATURAL JOIN pais) dest


ON tbl.id_vuelo = ori.id_vuelo and tbl.id_vuelo = dest.id_vuelo WHERE id_usuario = '$id_usuario'") ;

        return ($query->fetchAll());
    
    }

    public function obtenerAutos($id_usuario){

      
      $query = $this->_db->query("SELECT * FROM auto NATURAL JOIN auto_agencia NATURAL JOIN auto_gama NATURAL JOIN gama NATURAL JOIN auto_marca NATURAL JOIN marca NATURAL JOIN agencia NATURAL JOIN auto_reserva NATURAL JOIN reserva_auto NATURAL JOIN usuario_reserva_auto res 
        
        INNER JOIN 

        (SELECT id_usuario FROM usuario WHERE id_usuario = '$id_usuario') u
        ON u.id_usuario = res.id_usuario") ;
        return ($query->fetchAll());
    
    }

    public function obtenerHabitaciones($id_usuario){

      
      $query = $this->_db->query("SELECT * FROM habitacion NATURAL JOIN habitacion_hotel  NATURAL JOIN habitacion_reserva_habitacion NATURAL JOIN reserva_habitacion NATURAL JOIN usuario_reserva_habitacion res


        NATURAL JOIN

         (SELECT id_hotel, nombre FROM hotel) aaa 


        INNER JOIN

        (SELECT id_usuario FROM usuario WHERE id_usuario = '$id_usuario') u
        ON u.id_usuario = res.id_usuario 

        ") ;



       
      
        return ($query->fetchAll());
    
    }

    public function quitarReservaVuelo($id_reserva_vuelo,$id_vuelo,$cantidad_economica, $cantidad_primera, $cantidad_business){
      //Elimino la reserva del vuelo
      $this->_db->prepare("DELETE FROM reserva_vuelo WHERE id_reserva_vuelo=:id_reserva_vuelo")
                ->execute(
                        array(
                           ':id_reserva_vuelo' => $id_reserva_vuelo
                         
                        ));
      //Devuelvo stock al vuelo
      $this->_db->prepare("UPDATE vuelo SET capacidad_economica=capacidad_economica+:cantidad_economica, capacidad_primera=capacidad_primera+:cantidad_primera, capacidad_business=capacidad_business+:cantidad_business WHERE id_vuelo=:id_vuelo")
                ->execute(
                        array(
                           ':cantidad_economica' => $cantidad_economica,
                           ':cantidad_primera' => $cantidad_primera,
                           ':cantidad_business' => $cantidad_business,
                           ':id_vuelo' => $id_vuelo
                         
                        ));


    }
    public function quitarReservaAuto($id_reserva_auto){
      //Elimino la reserva del auto
      $this->_db->prepare("DELETE FROM reserva_auto WHERE id_reserva_auto=:id_reserva_auto")
                ->execute(
                        array(
                           ':id_reserva_auto' => $id_reserva_auto
                         
                        ));
      $this->_db->prepare("DELETE FROM auto_reserva WHERE id_reserva_auto=:id_reserva_auto")
                ->execute(
                        array(
                           ':id_reserva_auto' => $id_reserva_auto
                         
                        ));
      $this->_db->prepare("DELETE FROM usuario_reserva_auto WHERE id_reserva_auto=:id_reserva_auto")
                ->execute(
                        array(
                           ':id_reserva_auto' => $id_reserva_auto
                         
                        ));


    }
    public function quitarReservaHabitacion($id_reserva_habitacion){
      //Elimino la reserva de la habitacion

      $this->_db->prepare("DELETE FROM reserva_habitacion WHERE id_reserva_habitacion=:id_reserva_habitacion")
                ->execute(
                        array(
                           ':id_reserva_habitacion' => $id_reserva_habitacion
                         
                        ));
      $this->_db->prepare("DELETE FROM usuario_reserva_habitacion WHERE id_reserva_habitacion=:id_reserva_habitacion")
                ->execute(
                        array(
                           ':id_reserva_habitacion' => $id_reserva_habitacion
                         
                        ));
                $this->_db->prepare("DELETE FROM habitacion_reserva_habitacion WHERE id_reserva_habitacion=:id_reserva_habitacion")
                ->execute(
                        array(
                           ':id_reserva_habitacion' => $id_reserva_habitacion
                         
                        ));


    }

    // Pasa los servicios que estan como "esperando consumir" a "consumido", esta funcion se dispara cada vez que un usuario se loguea correctamente
    public function consumirServicios($id_usuario){


      //primero acumulo los puntos de todo lo que tengo esperando consumir ya paso la fecha

      $cantPuntosASumar=0;
      $stmt=$this->_db->prepare("SELECT SUM(puntos_a_acumular) as suma FROM reserva_vuelo NATURAL JOIN vuelo_reserva_vuelo NATURAL JOIN vuelo NATURAL JOIN usuario_reserva_vuelo WHERE estado ='esperando consumir' AND id_usuario=:id_usuario AND salida < NOW()");
                $stmt->execute(
                        array(
                           ':id_usuario' => $id_usuario
                        ));
      $cantPuntosASumar+=$stmt->fetch()['suma'];

      $stmt=$this->_db->prepare("SELECT SUM(puntos_a_acumular) as suma FROM reserva_habitacion NATURAL JOIN habitacion_reserva_habitacion NATURAL JOIN habitacion NATURAL JOIN usuario_reserva_habitacion WHERE estado ='esperando consumir' AND id_usuario=:id_usuario AND desde < NOW()");
                $stmt->execute(
                        array(
                           ':id_usuario' => $id_usuario
                        ));
      $cantPuntosASumar+=$stmt->fetch()['suma'];

      

      $stmt=$this->_db->prepare("SELECT SUM(puntos_a_acumular) as suma FROM reserva_auto NATURAL JOIN auto_reserva NATURAL JOIN auto NATURAL JOIN usuario_reserva_auto WHERE estado ='esperando consumir' AND id_usuario=:id_usuario AND desde < NOW()");
                $stmt->execute(
                        array(
                           ':id_usuario' => $id_usuario
                        ));

      


      $cantPuntosASumar+=$stmt->fetch()['suma'];

  ///sumo al usuario todos los puntos que correspondan
      $this->_db->prepare("UPDATE usuario SET puntos =puntos+:cantASumar WHERE  id_usuario=:id_usuario")
                ->execute(
                        array(
                           ':id_usuario' => $id_usuario,
                           ':cantASumar' => $cantPuntosASumar,
                         
                        ));

      //luego paso a consumido
      $this->_db->prepare("UPDATE reserva_vuelo NATURAL JOIN vuelo_reserva_vuelo NATURAL JOIN usuario_reserva_vuelo NATURAL JOIN vuelo SET estado ='consumido' WHERE  id_usuario=:id_usuario AND salida < NOW()")
                ->execute(
                        array(
                           ':id_usuario' => $id_usuario
                         
                        ));
    

    $this->_db->prepare("UPDATE reserva_auto NATURAL JOIN auto_reserva NATURAL JOIN usuario_reserva_auto NATURAL JOIN auto SET estado ='consumido' WHERE  id_usuario=:id_usuario AND desde < NOW()")
                ->execute(
                        array(
                           ':id_usuario' => $id_usuario
                         
                        ));
    

    $this->_db->prepare("UPDATE reserva_habitacion NATURAL JOIN habitacion_reserva_habitacion NATURAL JOIN usuario_reserva_habitacion NATURAL JOIN habitacion SET estado ='consumido' WHERE  id_usuario=:id_usuario AND desde < NOW()")
                ->execute(
                        array(
                           ':id_usuario' => $id_usuario
                         
                        ));
    

  }

  public function cancelarReservaVuelo($id_reserva_vuelo,$id_vuelo,$cantidad_economica, $cantidad_primera, $cantidad_business){
    $this->_db->prepare("UPDATE reserva_vuelo NATURAL JOIN vuelo_reserva_vuelo NATURAL JOIN vuelo SET estado ='cancelado', capacidad_economica=capacidad_economica+:cantidad_economica, capacidad_primera=capacidad_primera+:cantidad_primera, capacidad_business=capacidad_business+:cantidad_business  WHERE  id_reserva_vuelo=:id_reserva_vuelo AND id_vuelo=:id_vuelo")
                ->execute(
                        array(
                           ':id_reserva_vuelo' => $id_reserva_vuelo,
                           ':cantidad_economica' => $cantidad_economica,
                           ':cantidad_primera' => $cantidad_primera,
                           ':cantidad_business' => $cantidad_business,
                           ':id_vuelo' => $id_vuelo
                         
                        ));

  }

  public function cancelarReservaAuto($id_reserva_auto){
    $this->_db->prepare("UPDATE reserva_auto SET estado ='cancelado' WHERE  id_reserva_auto=:id_reserva_auto")
                ->execute(
                        array(
                           ':id_reserva_auto' => $id_reserva_auto
                        ));

  }

  public function cancelarReservaHabitacion($id_reserva_habitacion){
    $this->_db->prepare("UPDATE reserva_habitacion SET estado ='cancelado' WHERE  id_reserva_habitacion=:id_reserva_habitacion")
                ->execute(
                        array(
                           ':id_reserva_habitacion' => $id_reserva_habitacion
                        ));

  }


  public function resetearContrasena($nombre_de_usuario, $contrasena_nueva){
    $this->_db->prepare("UPDATE usuario SET contrasena=:contrasena_nueva WHERE  nombre_de_usuario=:nombre_de_usuario")
                ->execute(  
                        array(
                           ':nombre_de_usuario' => $nombre_de_usuario,
                           ':contrasena_nueva' => $contrasena_nueva

                        ));

  }


  public function getPuntos($nombre_de_usuario){
    $stmt = $this->_db->prepare('select puntos from usuario where nombre_de_usuario=:nombre_de_usuario');                                                                                                                                                     
    $stmt->execute( array(
                           ':nombre_de_usuario' => $nombre_de_usuario,
                           

                        ));
    $puntos = $stmt->fetch();


    return $puntos;

  }



  /* $this->usuarioModel->restarPuntos($nombre_de_usuario,$_POST['puntos_a_usar']);
        //acumular puntos al usuario (agregar un campo a cada reserva)  y pasar lo que tenga en carrito a esperando consumir
        $this->usuarioModel->pagarCompra($nombre_de_usuario);
        $this->usuarioModel->sumarPuntos($nombre_de_usuario,$_POST['puntos_a_acumular']);
        */
  public function restarPuntos($nombre_de_usuario,$puntos){
    $stmt = $this->_db->prepare('update usuario set puntos=puntos-:puntos where nombre_de_usuario=:nombre_de_usuario');       

    $stmt->execute( array(
                           ':nombre_de_usuario' => $nombre_de_usuario,
                           ':puntos' => $puntos,
                           

                        ));
  }

  public function sumarPuntos($nombre_de_usuario,$puntos){
    $stmt = $this->_db->prepare('update usuario set puntos=puntos+:puntos where nombre_de_usuario=:nombre_de_usuario');       

    $stmt->execute( array(
                           ':nombre_de_usuario' => $nombre_de_usuario,
                           ':puntos' => $puntos,
                           

                        ));
  }

  public function pagarCompra($idUsuario, $pesos_por_puntos, $abona_con_tarjeta){
   
    $stmt = $this->_db->prepare("update reserva_vuelo natural join usuario_reserva_vuelo set estado=:estado, puntos_a_acumular=:puntos_a_acumular where id_usuario=:id_usuario and estado='en carrito'");       

    $stmt->execute( array(
                           ':id_usuario' => $idUsuario,
                           ':estado' => 'esperando consumir',
                           ':puntos_a_acumular' => ($abona_con_tarjeta*$pesos_por_puntos),
                        ));


    $stmt = $this->_db->prepare("update reserva_auto natural join usuario_reserva_auto set estado=:estado, puntos_a_acumular=:puntos_a_acumular where id_usuario=:id_usuario and estado='en carrito'");       

    $stmt->execute( array(
                           ':id_usuario' => $idUsuario,
                           ':estado' => 'esperando consumir',
                           ':puntos_a_acumular' => ($abona_con_tarjeta*$pesos_por_puntos),
                        ));


   $stmt = $this->_db->prepare("update reserva_habitacion natural join usuario_reserva_habitacion set estado=:estado, puntos_a_acumular=:puntos_a_acumular where id_usuario=:id_usuario and estado='en carrito'");       

    $stmt->execute( array(
                           ':id_usuario' => $idUsuario,
                           ':estado' => 'esperando consumir',
                           ':puntos_a_acumular' => ($abona_con_tarjeta*$pesos_por_puntos),
                        ));


  }

  public function preguntaSecreta($nombre_de_usuario){
    $stmt = $this->_db->prepare('select pregunta_secreta from usuario where nombre_de_usuario=:nombre_de_usuario');                                                                                                                                                     
    $stmt->execute( array(
                           ':nombre_de_usuario' => $nombre_de_usuario,
                           

                        ));
    $pregunta_secreta = $stmt->fetch();

    
    return $pregunta_secreta['pregunta_secreta'];
  }
  public function restablecerContrasenaConRespuesta($nombre_de_usuario, $respuesta, $contrasena_nueva){
      $stmt = $this->_db->prepare('select respuesta_secreta from usuario where nombre_de_usuario=:nombre_de_usuario');                                                                                                                                                     
    $stmt->execute( array(
                           ':nombre_de_usuario' => $nombre_de_usuario,
                           

                        ));
    $respuesta_secreta = $stmt->fetch();

    
    if ($respuesta_secreta['respuesta_secreta'] == $respuesta){
        $this->_db->prepare('update usuario set contrasena=:contrasena_nueva where nombre_de_usuario=:nombre_de_usuario') 
        ->execute( array(
                           ':nombre_de_usuario' => $nombre_de_usuario,
                           ':contrasena_nueva' => $contrasena_nueva,
                           
                           

                        ));
      return true;
    }
    return false;

  }


}





?>