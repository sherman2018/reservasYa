<?php 

class hotelModel extends Model
{
    public function __construct() {
        parent::__construct();
    }

    public function getHoteles(){
        $gsent = $this->_db->prepare("SELECT * FROM hotel");
        $gsent->execute();
        return $gsent->fetchAll();
    }

    public function getHabitaciones($id_hotel){
        $gsent = $this->_db->prepare("SELECT COUNT(id_habitacion) as cantidad, capacidad FROM habitacion natural join habitacion_hotel where (id_hotel = :id_hotel AND eliminado = '0') group by(capacidad)");
        $gsent->execute(array(':id_hotel'=>$id_hotel));
        
        return $gsent->fetchAll();
    }

    public function getTodosLosServicios(){
        $gsent = $this->_db->prepare("SELECT id_servicio, tipo as servicio FROM servicio");
        $gsent->execute();
        return $gsent->fetchAll();
    }
	public function crear($nombre, $id_ciudad, $estrellas, $precio, $servicios){
        $this->_db->prepare("INSERT INTO hotel (nombre, estrellas, precio_por_persona) VALUES (:nombre, :estrellas, :precio);")
                ->execute(
                        array(
                           ':nombre' => $nombre,
                           ':estrellas' => $estrellas,
                           ':precio' => $precio
                        ));
                $id_hotel = $this->_db->lastInsertId();



        $this->_db->prepare("INSERT INTO hotel_ciudad (id_hotel, id_ciudad) VALUES (:id_hotel, :id_ciudad);")
                ->execute(
                        array(
                           ':id_hotel' => $id_hotel,
                           ':id_ciudad' => $id_ciudad
                        ));
		foreach ($servicios as $id_servicio) {
			$this->_db->prepare("INSERT INTO hotel_servicio (id_hotel, id_servicio) VALUES (:id_hotel, :id_servicio);")
		                ->execute(
		                        array(
		                           ':id_hotel' => $id_hotel,
		                           ':id_servicio' => $id_servicio
		                        ));
		}
        return ($id_hotel." - ".$nombre);
    }
    public function puntuar($id_hotel,$puntaje){
        $this->_db->prepare("UPDATE hotel SET puntos=puntos+:puntaje, veces_puntuado=veces_puntuado+ 1 WHERE id_hotel=:id_hotel")
                            ->execute(
                                    array(
                                       ':id_hotel' => $id_hotel,
                                       ':puntaje' => $puntaje
                                    ));        
    }
    

    public function obtenerServicios($id_hotel){
        $stmt=$this->_db->prepare("SELECT tipo FROM servicio natural join hotel_servicio WHERE id_hotel=:id_hotel");
        $stmt->execute(
                array(
                   ':id_hotel' => $id_hotel,
                   
                ));

        
        return $stmt->fetchAll();

    }
    
}
?>