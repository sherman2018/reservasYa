<?php 

class configuracionModel extends Model
{
    public function __construct() {
        parent::__construct();
    }
    
    public function getParametros(){
      $query = $this->_db->query("SELECT * from configuracion");
        return ($query->fetchAll());

    }
    public function modificarParametro($variable, $valor){
      $gsent = $this->_db->prepare('UPDATE configuracion SET valor=? where variable=?');
      $gsent->execute(array($valor, $variable));
    }

    public function getConfiguraciones(){
        $gsent = $this->_db->prepare("SELECT * FROM configuracion");
        $gsent->execute();
        return $gsent->fetchAll();
    }
  
    
    
}




?>