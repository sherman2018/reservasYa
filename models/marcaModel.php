<?php 

class marcaModel extends Model
{
    public function __construct() {
        parent::__construct();
    }

    public function getMarcas(){
        $gsent = $this->_db->prepare("SELECT * FROM marca");
        $gsent->execute();
        return $gsent->fetchAll();
    }
}
?>