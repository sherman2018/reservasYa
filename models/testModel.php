<?php 

class TestModel extends Model
{
    public function __construct() {
        parent::__construct();
    }
    
  
    public function iniciar()
    {
        $gsent = $this->_db->query("
TRUNCATE TABLE aerolinea;
TRUNCATE TABLE agencia;
TRUNCATE TABLE agencia_ciudad;
TRUNCATE TABLE agencia_franquicia;
TRUNCATE TABLE auto;
TRUNCATE TABLE auto_agencia;
TRUNCATE TABLE auto_gama;
TRUNCATE TABLE auto_marca;
TRUNCATE TABLE auto_reserva;
TRUNCATE TABLE ciudad;
TRUNCATE TABLE ciudad_provincia;
TRUNCATE TABLE configuracion;
TRUNCATE TABLE franquicia;
TRUNCATE TABLE gama;
TRUNCATE TABLE habitacion;
TRUNCATE TABLE habitacion_hotel;
TRUNCATE TABLE habitacion_reserva_habitacion;
TRUNCATE TABLE hotel;
TRUNCATE TABLE hotel_ciudad;
TRUNCATE TABLE hotel_servicio;
TRUNCATE TABLE marca;
TRUNCATE TABLE pais;
TRUNCATE TABLE permisos;
TRUNCATE TABLE permisos_role;
TRUNCATE TABLE permisos_usuario;
TRUNCATE TABLE provincia;
TRUNCATE TABLE provincia_pais;
TRUNCATE TABLE reserva_auto;
TRUNCATE TABLE reserva_habitacion;
TRUNCATE TABLE reserva_vuelo;
TRUNCATE TABLE servicio;
TRUNCATE TABLE usuario;
TRUNCATE TABLE usuarios;
TRUNCATE TABLE usuario_reserva_auto;
TRUNCATE TABLE usuario_reserva_habitacion;
TRUNCATE TABLE usuario_reserva_vuelo;
TRUNCATE TABLE vuelo;
TRUNCATE TABLE vuelo_aerolinea;
TRUNCATE TABLE vuelo_desde;
TRUNCATE TABLE vuelo_hasta;
TRUNCATE TABLE vuelo_reserva_vuelo;

INSERT INTO aerolinea (id_aerolinea, aerolinea) VALUES
(1, 'Aerolineas Argentinas'),
(2, 'AlItalia');


INSERT INTO agencia (id_agencia, nombre) VALUES
(1, 'Avis Sucursal Bs. As.'),
(2, 'Avis Sucursal Montevideo'),
(3, 'Avis Sucursal \r\nParis'),
(4, 'Avis Sucursal Berlin');


INSERT INTO agencia_ciudad (id_agencia, id_ciudad) VALUES
(1, 1),
(2, 3),
(3, 6),
(4, 7);


INSERT INTO agencia_franquicia (id_agencia, id_franquicia) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1);


INSERT INTO auto (id_auto, patente, autonomia, capacidad, eliminado) VALUES
(1, '', 500, 5, 0),
(2, '', 600, 5, 0);


INSERT INTO auto_agencia (id_auto, id_agencia) VALUES
(1, 1),
(2, 3);


INSERT INTO auto_gama (id_auto, id_gama) VALUES
(1, 2),
(2, 1);


INSERT INTO auto_marca (id_auto, id_marca) VALUES
(1, 1),
(2, 2);


INSERT INTO auto_reserva (id_reserva_auto, id_auto) VALUES
(1, 1),
(2, 2);


INSERT INTO ciudad (id_ciudad, ciudad) VALUES
(1, 'Buenos Aires'),
(2, 'Mar del Plata'),
(3, 'Montevideo'),
(4, 'Lisboa'),
(5, 'Madrid'),
(6, 'Paris'),
(7, 'Berlin'),
(8, 'Roma');


INSERT INTO ciudad_provincia (id_ciudad, id_provincia) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 3),
(5, 4),
(6, 5),
(7, 6),
(8, 7);


INSERT INTO configuracion (id_configuracion, variable, valor) VALUES
(1, 'pesos_por_puntos', '0.5'),
(2, 'puntos_por_pesos', '0.75'),
(3, 'max_gap', '4'),
(5, 'factor_primera', '50'),
(6, 'factor_business', '25'),
(7, 'factor_devolucion_por_dia', '10'),
(8, 'factor_descuento_escala', '50'),
(9, 'items_por_pagina', '2');


INSERT INTO franquicia (id_franquicia, franquicia) VALUES
(1, 'Avis');


INSERT INTO gama (id_gama, gama, precio_por_dia) VALUES
(1, 'alta', 1300),
(2, 'media', 1200),
(3, 'baja', 1000);


INSERT INTO habitacion (id_habitacion, capacidad, eliminado) VALUES
(1, 5, 0),
(2, 3, 0),
(3, 3, 0);


INSERT INTO habitacion_hotel (id_habitacion, id_hotel) VALUES
(1, 1),
(2, 2),
(3, 3);


INSERT INTO hotel (id_hotel, nombre, estrellas, puntos, veces_puntuado, precio_por_persona) VALUES
(1, 'Roma', 3, 9, 2, 450),
(2, 'Italia', 2, 15, 3, 600),
(3, 'Sudestada', 5, 0, 0, 200);


INSERT INTO hotel_ciudad (id_hotel, id_ciudad) VALUES
(1, 8),
(2, 8),
(3, 6);


INSERT INTO hotel_servicio (id_servicio, id_hotel) VALUES
(1, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 3);


INSERT INTO marca (id_marca, marca) VALUES
(1, 'Volkswagen'),
(2, 'Peugeot');

INSERT INTO pais (id_pais, pais) VALUES
(1, 'Argentina'),
(2, 'Uruguay'),
(3, 'Portugal'),
(4, 'España'),
(5, 'Francia'),
(6, 'Alemania'),
(7, 'Italia');


INSERT INTO permisos_role (role, permiso, valor) VALUES
(1, 1, 1),
(1, 2, 1),
(1, 3, 1),
(1, 4, 1),
(2, 2, 1),
(2, 3, 1),
(2, 4, 1),
(3, 2, 1),
(3, 3, 1),
(4, 2, 1);

INSERT INTO provincia (id_provincia, provincia) VALUES
(1, 'Buenos Aires'),
(2, 'Montevideo'),
(3, 'Lisboa'),
(4, 'Comunidad de Madrid'),
(5, 'Ile de France'),
(6, 'Berlin'),
(7, 'Lazio');


INSERT INTO provincia_pais (id_provincia, id_pais) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7);


INSERT INTO reserva_auto (id_reserva_auto, estado, precio_abonado, puntos_a_acumular, desde, hasta, otroPais, id_agencia_origen, id_agencia_destino, fecha_creacion) VALUES
(1, 'esperando consumir', 6000, 0, '2019-03-20 00:00:00', '2019-03-26 00:00:00', 1, 0, 0, '2018-12-08 22:26:11'),
(2, 'esperando consumir', 3900, 0, '2019-10-10 00:00:00', '2019-10-14 00:00:00', 1, 0, 0, '2018-12-08 22:27:50');


INSERT INTO servicio (id_servicio, tipo) VALUES
(1, 'Cancha de futbol'),
(2, 'Pileta'),
(3, 'Gym');

INSERT INTO usuario (id_usuario, nombre_de_usuario, mail, contrasena, nombre, apellido, dni, puntos, nro_tarjeta, pregunta_secreta, respuesta_secreta, admin, comercial, cliente) VALUES
(1, 'root', 'root@root.com', 'pass', 'Marcos', 'Gomez', '12456898', 0, NULL, 'lala', 'lal', 1, 1, 1),
(2, 'pepe', 'pepee@pepe', '123', 'prueba', 'pruebita', '12356988', 0, NULL, 'jb?', 'lnl', 0, 0, 1),
(3, 'userA', 'usera@gmail.com', 'pass', 'User', 'A', '', 0, NULL, '', '', 0, 1, 0),
(4, 'userB', 'userb@yahoo.com', 'pass', 'User', 'B', '25265234', 0, NULL, '¿Como se llama mi perro?', 'Tomba', 0, 0, 1),
(5, 'userC', 'userc@hotmail.com', 'pass', 'User', 'C', '24568985', 0, NULL, '¿Cual es mi color favorito?', 'rojo', 0, 0, 1),
(6, 'userD', 'userd@gmail.com', 'pass', 'User', 'D', '12456898', 0, NULL, 'lala', 'lal', 1, 0, 0);


INSERT INTO usuarios (id, nombre, usuario, pass, email, role, estado, fecha, codigo) VALUES
(1, 'nombre1', 'admin', 'd1b254c9620425f582e27f0044be34bee087d8b4', 'admin@admin.adm', 1, 1, '0000-00-00 00:00:00', 1963007335),
(2, 'usuario1', 'usuario1', 'd1b254c9620425f582e27f0044be34bee087d8b4', 'usuario1@user.com', 2, 1, '2012-03-21 20:53:07', 1963007335),
(3, 'usuario2', 'usuario2', 'd1b254c9620425f582e27f0044be34bee087d8b4', 'usuario2@user.com', 3, 1, '2012-03-21 20:57:01', 1963007335);

INSERT INTO usuario_reserva_auto (id_usuario, id_reserva_auto) VALUES
(4, 1),
(4, 2);


INSERT INTO vuelo (id_vuelo, salida, llegada, capacidad_primera, capacidad_business, capacidad_economica, precio, eliminado) VALUES
(1, '2019-01-10 13:00:00', '2019-01-11 03:00:00', 10, 20, 100, 30000, 0),
(2, '2019-01-10 13:00:00', '2019-01-10 15:00:00', 0, 10, 50, 2000, 0),
(3, '2019-01-10 13:00:00', '2019-01-10 15:00:00', 10, 10, 90, 28000, 0),
(4, '2019-01-10 19:00:00', '2019-01-11 07:00:00', 10, 10, 90, 28000, 0),
(5, '2019-01-11 04:00:00', '2019-01-11 07:00:00', 20, 10, 50, 5800, 0),
(6, '2019-01-11 08:00:00', '2019-01-11 11:00:00', 20, 10, 50, 6500, 0),
(7, '2019-04-15 05:00:00', '2019-04-15 18:00:00', 0, 0, 10, 25000, 0),
(8, '2019-04-15 07:00:00', '2019-04-15 21:00:00', 22, 20, 20, 19000, 0),
(9, '2019-04-15 12:00:00', '2019-04-16 06:00:00', 13, 18, 15, 22000, 0);

INSERT INTO vuelo_aerolinea (id_vuelo, id_aerolinea) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 1),
(5, 1),
(6, 1),
(7, 2),
(8, 2),
(9, 2);


INSERT INTO vuelo_desde (id_vuelo, id_ciudad) VALUES
(1, 1),
(2, 1),
(3, 3),
(4, 3),
(5, 4),
(6, 4),
(7, 1),
(8, 1),
(9, 1);


INSERT INTO vuelo_hasta (id_vuelo, id_ciudad) VALUES
(1, 4),
(2, 3),
(3, 4),
(4, 4),
(5, 5),
(6, 5),
(7, 5),
(8, 5),
(9, 5);


");
        try {
        	$gsent->execute();
  		echo "Base de datos lista para iniciar test <a href='../'>Iniciar Test</a>";die;
        	
        } catch (PDOException $e) {
        	
        }
        
    }
   
    
    
}




?>