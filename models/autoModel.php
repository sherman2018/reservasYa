<?php 
require_once(ROOT . DS. 'models'. DS.'ciudadModel.php');

class autoModel extends Model
{
    private $ciudadModel;
    public function __construct() {
        parent::__construct();
        $this->ciudadModel = new ciudadModel();
    }

    
    public function crear($cantidad,$id_agencia, $capacidad, $id_marca, $id_gama, $autonomia){   
        $this->_db->prepare("INSERT INTO auto (capacidad, autonomia) VALUES (:capacidad, :autonomia)")
                ->execute(
                        array(
                          
                           ':capacidad' => $capacidad,
                           ':autonomia' => $autonomia
                        ));
                $id_auto = $this->_db->lastInsertId();



        $this->_db->prepare("INSERT INTO auto_agencia (id_auto, id_agencia) VALUES (:id_auto, :id_agencia);")
                ->execute(
                        array(
                           ':id_auto' => $id_auto,
                           ':id_agencia' => $id_agencia
                        ));

        $this->_db->prepare("INSERT INTO auto_marca (id_auto, id_marca) VALUES (:id_auto, :id_marca);")
                ->execute(
                        array(
                           ':id_auto' => $id_auto,
                           ':id_marca' => $id_marca
                        ));

         $this->_db->prepare("INSERT INTO auto_gama (id_auto, id_gama) VALUES (:id_auto, :id_gama);")
                ->execute(
                        array(
                           ':id_auto' => $id_auto,
                           ':id_gama' => $id_gama
                        ));
         

    }

    
    public function eliminar($id_agencia, $gama, $marca, $cantidad_a_eliminar, $capacidad){
       $stmt = $this->_db->prepare("UPDATE auto SET eliminado='1' WHERE capacidad=:capacidad and eliminado='0' and id_auto in (select id_auto from auto_agencia natural join auto_gama natural join gama natural join auto_marca natural join marca where id_agencia=:id_agencia and gama=:gama and marca=:marca) LIMIT $cantidad_a_eliminar");
                $stmt->execute(
                        array(
                           
                           ':id_agencia' => $id_agencia,
                           ':capacidad' => $capacidad,
                           ':gama' => $gama,
                           ':marca' => $marca,
                        ));


    } 
    
    public function buscarAuto($origen, $destino, $fechaInicio, $fechaFin, $cantidadPersonas){
        $origenCiudad = $this->ciudadModel->getNombre($origen);
        $destinoCiudad = $this->ciudadModel->getNombre($destino);

        $origenPais = $this->ciudadModel->getPais($origen);
        $destinoPais =$this->ciudadModel->getPais($destino);

        $fechaInicio = new DateTime($fechaInicio);
        $fechaInicio->modify('+1 second');
        $fechaInicio=$fechaInicio->format('Y-m-d H:i:s');

        $fechaFin = new DateTime($fechaFin);
        $fechaFin->modify('-1 second');
        $fechaFin=$fechaFin->format('Y-m-d H:i:s');


        $sql="SELECt * FROM agencia NATURAL JOIN auto_agencia NATURAL JOIN auto NATURAL JOIN auto_gama NATURAL JOIN gama NATURAL JOIN auto_marca NATURAL JOIN marca NATURAL JOIN agencia_franquicia NATURAL JOIN agencia_ciudad WHERE id_ciudad=:ciudadOrigen AND eliminado=0 AND id_franquicia IN (select distinct(id_franquicia) FROM agencia_franquicia NATURAL JOIN agencia_ciudad WHERE (capacidad >= :capacidad) AND id_ciudad=:ciudadDestino) AND id_auto NOT IN (SELECT distinct(id_auto) FROM auto_reserva NATURAL JOIN reserva_auto WHERE (((desde  BETWEEN :desde AND :hasta) OR (hasta  BETWEEN :desde AND :hasta) OR (:desde  BETWEEN desde AND hasta) OR (:hasta  BETWEEN desde AND hasta)) AND (estado <> 'cancelado'))) order by gama.precio_por_dia";








           $pdoConsulta = $this->_db->prepare($sql);
           $params = array(":ciudadOrigen" => $origen, ":ciudadDestino" => $destino, ":desde" => $fechaInicio, ":hasta" => $fechaFin, ":capacidad" => $cantidadPersonas);
           
           $pdoConsulta->execute($params);
           
           $result = $pdoConsulta->fetchAll(PDO::FETCH_ASSOC);
           foreach ($result as &$key ) {
                $key['origenCiudad'] = $origenCiudad;
                $key['destinoCiudad'] = $destinoCiudad;
                $key['origenPais'] = $origenPais;
                $key['destinoPais'] = $destinoPais;
                
               

           }
          
            
            return $result;
        
    }


    public function agregarCarrito($estado, $precio_abonado, $desde, $fecha_hasta, $id_agencia_origen, $id_agencia_destino, $otroPais, $id_usuario, $id_auto){
        $this->_db->prepare("INSERT INTO reserva_auto (estado, precio_abonado, desde, hasta, id_agencia_origen, id_agencia_destino, otroPais) VALUES (:estado, :precio_abonado, :desde, :hasta, :id_agencia_origen, :id_agencia_destino, :otroPais);")
                ->execute(
                            array(
                                    ':estado' => $estado,
                                    ':precio_abonado' => $precio_abonado,
                                    ':desde' => $desde,
                                    ':hasta' => $fecha_hasta,
                                    ':id_agencia_origen' => $id_agencia_origen,
                                    ':id_agencia_destino' => $id_agencia_destino,
                                    ':otroPais' => $otroPais
                                ));
                $id_reserva_auto= $this->_db->lastInsertId();


        $this->_db->prepare("INSERT INTO usuario_reserva_auto (id_usuario, id_reserva_auto) VALUES (:id_usuario, :id_reserva_auto);")
            ->execute(
                    array(
                        ':id_usuario' => $id_usuario,
                        ':id_reserva_auto' => $id_reserva_auto
                        ));


        $this->_db->prepare("INSERT INTO auto_reserva (id_reserva_auto, id_auto) VALUES (:id_reserva_auto, :id_auto);")
            ->execute(
                    array(
                            ':id_reserva_auto' => $id_reserva_auto,
                            ':id_auto' => $id_auto
                            ));

    }
    
    
}




?>