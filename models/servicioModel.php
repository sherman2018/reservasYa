<?php 

class servicioModel extends Model
{
    public function __construct() {
        parent::__construct();
    }
    public function limpiarCarrito(){

		date_default_timezone_set('America/Argentina/Buenos_Aires');
    	$actual = new DateTime();
    	$actual->modify('-1 hour');

    	$this->_db->prepare("DELETE FROM reserva_habitacion where ((estado like '%carrito%') AND (fecha_creacion < ?))")
                ->execute(
                        array(                      
							 $actual->format('Y-m-d H:i:s')
                        ));

        //Falta devolver stock para los vuelos.

        $stmt = $this->_db->prepare("SELECT id_vuelo, SUM(cantidad_economica) as cantEconomica, SUM(cantidadad_primera) as cantPrimera, SUM(cantidad_business) as cantBusiness FROM reserva_vuelo natural join vuelo_reserva_vuelo where ((estado like '%carrito%') AND (fecha_creacion < ?)) group by id_vuelo");
        $stmt->execute(
                        array(                      
                             $actual->format('Y-m-d H:i:s')
                        ));


        $cantidades_a_eliminar=$stmt->fetchAll();
        //cantidades a eliminar
        
        
        
        foreach ($cantidades_a_eliminar as $fila) {
            $id_vuelo=$fila['id_vuelo'];
            $cantidad_economica=$fila['cantEconomica'];
            $cantidad_primera=$fila['cantPrimera'];
            $cantidad_business=$fila['cantBusiness'];
            $this->_db->prepare("UPDATE vuelo SET capacidad_economica=capacidad_economica+:cantidadEconomica,capacidad_primera=capacidad_primera+:cantidadPrimera,capacidad_business=capacidad_business+:cantidadBusiness WHERE id_vuelo=:id_vuelo")->execute(
                            array(
                                ':cantidadEconomica' => $cantidad_economica,
                                ':cantidadPrimera' => $cantidad_primera,                      
                                 ':cantidadBusiness' => $cantidad_business,
                                 ':id_vuelo' => $id_vuelo
                            ));
        }

        


        $this->_db->prepare("DELETE FROM reserva_vuelo where ((estado like '%carrito%') AND (fecha_creacion < ?))")
                ->execute(
                        array(                      
							 $actual->format('Y-m-d H:i:s')
                        ));
        $this->_db->prepare("DELETE FROM reserva_auto where ((estado like '%carrito%') AND (fecha_creacion < ?))")
                ->execute(
                        array(                      
							 $actual->format('Y-m-d H:i:s')
                        ));
    }

    public function setReservaHabiacionPuntuada($id_reserva_habitacion){
        $this->_db->prepare("UPDATE reserva_habitacion SET puntuado='1' WHERE id_reserva_habitacion=:id_reserva_habitacion")
                            ->execute(
                                    array(
                                       ':id_reserva_habitacion' => $id_reserva_habitacion,
                                       
                                    ));   

    }
    
    
}




?>
