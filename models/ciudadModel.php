<?php 

class ciudadModel extends Model
{
    public function __construct() {
        parent::__construct();
    }
    
  
    public function getCiudades()
    {
        $gsent = $this->_db->prepare("SELECT * FROM ciudad");
        $gsent->execute();
        return $gsent->fetchAll();          
    }
   
   	public function getNombre($anId)
    {
        $gsent = $this->_db->prepare("SELECT ciudad FROM ciudad WHERE id_ciudad =:id");
        $gsent->execute(array(
                           
                           ':id' => $anId
                        ));
        return $gsent->fetch()[0];          
    }
    public function getPais($anId)
    {
        $gsent = $this->_db->prepare("SELECT id_pais FROM ciudad natural join ciudad_provincia natural join provincia_pais WHERE id_ciudad =:id");
        $gsent->execute(array(
                           
                           ':id' => $anId
                        ));
        return $gsent->fetch()[0];          
    }
    
    
}




?>