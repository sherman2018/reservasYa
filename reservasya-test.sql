-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-12-2018 a las 02:29:14
-- Versión del servidor: 10.1.29-MariaDB
-- Versión de PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `reservasya-test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aerolinea`
--

CREATE TABLE `aerolinea` (
  `id_aerolinea` int(11) NOT NULL,
  `aerolinea` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aerolinea`
--

INSERT INTO `aerolinea` (`id_aerolinea`, `aerolinea`) VALUES
(1, 'Aerolineas Argentinas'),
(2, 'AlItalia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agencia`
--

CREATE TABLE `agencia` (
  `id_agencia` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `agencia`
--

INSERT INTO `agencia` (`id_agencia`, `nombre`) VALUES
(1, 'Avis Sucursal Bs. As.'),
(2, 'Avis Sucursal Montevideo'),
(3, 'Avis Sucursal \r\nParis'),
(4, 'Avis Sucursal Berlin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agencia_ciudad`
--

CREATE TABLE `agencia_ciudad` (
  `id_agencia` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `agencia_ciudad`
--

INSERT INTO `agencia_ciudad` (`id_agencia`, `id_ciudad`) VALUES
(1, 1),
(2, 3),
(3, 6),
(4, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agencia_franquicia`
--

CREATE TABLE `agencia_franquicia` (
  `id_agencia` int(11) NOT NULL,
  `id_franquicia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `agencia_franquicia`
--

INSERT INTO `agencia_franquicia` (`id_agencia`, `id_franquicia`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto`
--

CREATE TABLE `auto` (
  `id_auto` int(11) NOT NULL,
  `patente` varchar(10) NOT NULL,
  `autonomia` int(11) NOT NULL,
  `capacidad` int(11) NOT NULL,
  `eliminado` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto`
--

INSERT INTO `auto` (`id_auto`, `patente`, `autonomia`, `capacidad`, `eliminado`) VALUES
(1, '', 500, 5, 0),
(2, '', 600, 5, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_agencia`
--

CREATE TABLE `auto_agencia` (
  `id_auto` int(11) NOT NULL,
  `id_agencia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto_agencia`
--

INSERT INTO `auto_agencia` (`id_auto`, `id_agencia`) VALUES
(1, 1),
(2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_gama`
--

CREATE TABLE `auto_gama` (
  `id_auto` int(11) NOT NULL,
  `id_gama` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto_gama`
--

INSERT INTO `auto_gama` (`id_auto`, `id_gama`) VALUES
(1, 2),
(2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_marca`
--

CREATE TABLE `auto_marca` (
  `id_auto` int(11) NOT NULL,
  `id_marca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto_marca`
--

INSERT INTO `auto_marca` (`id_auto`, `id_marca`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_reserva`
--

CREATE TABLE `auto_reserva` (
  `id_reserva_auto` int(11) NOT NULL,
  `id_auto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto_reserva`
--

INSERT INTO `auto_reserva` (`id_reserva_auto`, `id_auto`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id_ciudad` int(11) NOT NULL,
  `ciudad` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`id_ciudad`, `ciudad`) VALUES
(1, 'Buenos Aires'),
(2, 'Mar del Plata'),
(3, 'Montevideo'),
(4, 'Lisboa'),
(5, 'Madrid'),
(6, 'Paris'),
(7, 'Berlin'),
(8, 'Roma');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad_provincia`
--

CREATE TABLE `ciudad_provincia` (
  `id_ciudad` int(11) NOT NULL,
  `id_provincia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad_provincia`
--

INSERT INTO `ciudad_provincia` (`id_ciudad`, `id_provincia`) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 3),
(5, 4),
(6, 5),
(7, 6),
(8, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id_configuracion` int(11) NOT NULL,
  `variable` varchar(50) NOT NULL,
  `valor` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id_configuracion`, `variable`, `valor`) VALUES
(1, 'pesos_por_puntos', '0.5'),
(2, 'puntos_por_pesos', '0.75'),
(3, 'max_gap', '4'),
(5, 'factor_primera', '50'),
(6, 'factor_business', '25'),
(7, 'factor_devolucion_por_dia', '10'),
(8, 'factor_descuento_escala', '50'),
(9, 'items_por_pagina', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `franquicia`
--

CREATE TABLE `franquicia` (
  `id_franquicia` int(11) NOT NULL,
  `franquicia` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `franquicia`
--

INSERT INTO `franquicia` (`id_franquicia`, `franquicia`) VALUES
(1, 'Avis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gama`
--

CREATE TABLE `gama` (
  `id_gama` int(11) NOT NULL,
  `gama` varchar(10) NOT NULL,
  `precio_por_dia` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gama`
--

INSERT INTO `gama` (`id_gama`, `gama`, `precio_por_dia`) VALUES
(1, 'alta', 1300),
(2, 'media', 1200),
(3, 'baja', 1000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion`
--

CREATE TABLE `habitacion` (
  `id_habitacion` int(11) NOT NULL,
  `capacidad` int(11) NOT NULL,
  `eliminado` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habitacion`
--

INSERT INTO `habitacion` (`id_habitacion`, `capacidad`, `eliminado`) VALUES
(1, 5, 0),
(2, 3, 0),
(3, 3, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion_hotel`
--

CREATE TABLE `habitacion_hotel` (
  `id_habitacion` int(11) NOT NULL,
  `id_hotel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habitacion_hotel`
--

INSERT INTO `habitacion_hotel` (`id_habitacion`, `id_hotel`) VALUES
(1, 1),
(2, 2),
(3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion_reserva_habitacion`
--

CREATE TABLE `habitacion_reserva_habitacion` (
  `id_reserva_habitacion` int(11) NOT NULL,
  `id_habitacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel`
--

CREATE TABLE `hotel` (
  `id_hotel` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `estrellas` int(11) NOT NULL,
  `puntos` int(11) NOT NULL,
  `veces_puntuado` int(11) NOT NULL DEFAULT '0',
  `precio_por_persona` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hotel`
--

INSERT INTO `hotel` (`id_hotel`, `nombre`, `estrellas`, `puntos`, `veces_puntuado`, `precio_por_persona`) VALUES
(1, 'Roma', 3, 9, 2, 450),
(2, 'Italia', 2, 15, 3, 600),
(3, 'Sudestada', 5, 0, 0, 200);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel_ciudad`
--

CREATE TABLE `hotel_ciudad` (
  `id_hotel` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hotel_ciudad`
--

INSERT INTO `hotel_ciudad` (`id_hotel`, `id_ciudad`) VALUES
(1, 8),
(2, 8),
(3, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel_servicio`
--

CREATE TABLE `hotel_servicio` (
  `id_servicio` int(11) NOT NULL,
  `id_hotel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hotel_servicio`
--

INSERT INTO `hotel_servicio` (`id_servicio`, `id_hotel`) VALUES
(1, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `id_marca` int(11) NOT NULL,
  `marca` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`id_marca`, `marca`) VALUES
(1, 'Volkswagen'),
(2, 'Peugeot');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id_pais` int(11) NOT NULL,
  `pais` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id_pais`, `pais`) VALUES
(1, 'Argentina'),
(2, 'Uruguay'),
(3, 'Portugal'),
(4, 'España'),
(5, 'Francia'),
(6, 'Alemania'),
(7, 'Italia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id_permiso` int(11) NOT NULL,
  `permiso` varchar(100) NOT NULL,
  `key` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id_permiso`, `permiso`, `key`) VALUES
(1, 'Tareas de administracion', 'admin_access'),
(2, 'Agregar Posts', 'nuevo_post'),
(3, 'Editar Posts', 'editar_post'),
(4, 'Eliminar Posts', 'eliminar_post');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos_role`
--

CREATE TABLE `permisos_role` (
  `role` int(11) NOT NULL,
  `permiso` int(11) NOT NULL,
  `valor` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos_role`
--

INSERT INTO `permisos_role` (`role`, `permiso`, `valor`) VALUES
(1, 1, 1),
(1, 2, 1),
(1, 3, 1),
(1, 4, 1),
(2, 2, 1),
(2, 3, 1),
(2, 4, 1),
(3, 2, 1),
(3, 3, 1),
(4, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos_usuario`
--

CREATE TABLE `permisos_usuario` (
  `usuario` int(11) NOT NULL,
  `permiso` int(11) NOT NULL,
  `valor` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `id_provincia` int(11) NOT NULL,
  `provincia` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`id_provincia`, `provincia`) VALUES
(1, 'Buenos Aires'),
(2, 'Montevideo'),
(3, 'Lisboa'),
(4, 'Comunidad de Madrid'),
(5, 'Ile de France'),
(6, 'Berlin'),
(7, 'Lazio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia_pais`
--

CREATE TABLE `provincia_pais` (
  `id_provincia` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provincia_pais`
--

INSERT INTO `provincia_pais` (`id_provincia`, `id_pais`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_auto`
--

CREATE TABLE `reserva_auto` (
  `id_reserva_auto` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `precio_abonado` double NOT NULL,
  `puntos_a_acumular` int(11) NOT NULL DEFAULT '0',
  `desde` datetime NOT NULL,
  `hasta` datetime NOT NULL,
  `otroPais` tinyint(4) NOT NULL DEFAULT '0',
  `id_agencia_origen` int(11) NOT NULL,
  `id_agencia_destino` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva_auto`
--

INSERT INTO `reserva_auto` (`id_reserva_auto`, `estado`, `precio_abonado`, `puntos_a_acumular`, `desde`, `hasta`, `otroPais`, `id_agencia_origen`, `id_agencia_destino`, `fecha_creacion`) VALUES
(1, 'esperando consumir', 6000, 0, '2019-03-20 00:00:00', '2019-03-26 00:00:00', 1, 0, 0, '2018-12-08 22:26:11'),
(2, 'esperando consumir', 3900, 0, '2019-10-10 00:00:00', '2019-10-14 00:00:00', 1, 0, 0, '2018-12-08 22:27:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_habitacion`
--

CREATE TABLE `reserva_habitacion` (
  `id_reserva_habitacion` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `precio_abonado` double NOT NULL,
  `puntos_a_acumular` int(11) NOT NULL DEFAULT '0',
  `desde` datetime NOT NULL,
  `hasta` datetime NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `puntuado` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_vuelo`
--

CREATE TABLE `reserva_vuelo` (
  `id_reserva_vuelo` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `cantidad_economica` int(11) NOT NULL,
  `cantidadad_primera` int(11) NOT NULL,
  `cantidad_business` int(11) NOT NULL,
  `precio_abonado` double NOT NULL,
  `puntos_a_acumular` int(11) NOT NULL DEFAULT '0',
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `id_servicio` int(11) NOT NULL,
  `tipo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`id_servicio`, `tipo`) VALUES
(1, 'Cancha de futbol'),
(2, 'Pileta'),
(3, 'Gym');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombre_de_usuario` varchar(100) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `contrasena` varchar(50) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `dni` varchar(8) DEFAULT NULL,
  `puntos` int(11) DEFAULT '0',
  `nro_tarjeta` varchar(50) DEFAULT NULL,
  `pregunta_secreta` varchar(50) DEFAULT NULL,
  `respuesta_secreta` varchar(50) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT '0',
  `comercial` tinyint(1) DEFAULT '0',
  `cliente` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre_de_usuario`, `mail`, `contrasena`, `nombre`, `apellido`, `dni`, `puntos`, `nro_tarjeta`, `pregunta_secreta`, `respuesta_secreta`, `admin`, `comercial`, `cliente`) VALUES
(1, 'root', 'root@root.com', 'pass', 'Marcos', 'Gomez', '12456898', 0, NULL, 'lala', 'lal', 1, 1, 1),
(2, 'pepe', 'pepee@pepe', '123', 'prueba', 'pruebita', '12356988', 0, NULL, 'jb?', 'lnl', 0, 0, 1),
(3, 'userA', 'usera@gmail.com', 'pass', 'User', 'A', '', 0, NULL, '', '', 0, 1, 0),
(4, 'userB', 'userb@yahoo.com', 'pass', 'User', 'B', '25265234', 0, NULL, '¿Como se llama mi perro?', 'Tomba', 0, 0, 1),
(5, 'userC', 'userc@hotmail.com', 'pass', 'User', 'C', '24568985', 0, NULL, '¿Cual es mi color favorito?', 'rojo', 0, 0, 1),
(6, 'userD', 'userd@gmail.com', 'pass', 'User', 'D', '12456898', 0, NULL, 'lala', 'lal', 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(4) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` int(11) NOT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `codigo` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `usuario`, `pass`, `email`, `role`, `estado`, `fecha`, `codigo`) VALUES
(1, 'nombre1', 'admin', 'd1b254c9620425f582e27f0044be34bee087d8b4', 'admin@admin.adm', 1, 1, '0000-00-00 00:00:00', 1963007335),
(2, 'usuario1', 'usuario1', 'd1b254c9620425f582e27f0044be34bee087d8b4', 'usuario1@user.com', 2, 1, '2012-03-21 20:53:07', 1963007335),
(3, 'usuario2', 'usuario2', 'd1b254c9620425f582e27f0044be34bee087d8b4', 'usuario2@user.com', 3, 1, '2012-03-21 20:57:01', 1963007335);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_reserva_auto`
--

CREATE TABLE `usuario_reserva_auto` (
  `id_usuario` int(11) NOT NULL,
  `id_reserva_auto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_reserva_auto`
--

INSERT INTO `usuario_reserva_auto` (`id_usuario`, `id_reserva_auto`) VALUES
(4, 1),
(4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_reserva_habitacion`
--

CREATE TABLE `usuario_reserva_habitacion` (
  `id_reserva_habitacion` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_reserva_vuelo`
--

CREATE TABLE `usuario_reserva_vuelo` (
  `id_reserva_vuelo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo`
--

CREATE TABLE `vuelo` (
  `id_vuelo` int(11) NOT NULL,
  `salida` datetime NOT NULL,
  `llegada` datetime NOT NULL,
  `capacidad_primera` int(11) NOT NULL,
  `capacidad_business` int(11) NOT NULL,
  `capacidad_economica` int(11) NOT NULL,
  `precio` double NOT NULL,
  `eliminado` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vuelo`
--

INSERT INTO `vuelo` (`id_vuelo`, `salida`, `llegada`, `capacidad_primera`, `capacidad_business`, `capacidad_economica`, `precio`, `eliminado`) VALUES
(1, '2019-01-10 13:00:00', '2019-01-11 03:00:00', 10, 20, 100, 30000, 0),
(2, '2019-01-10 13:00:00', '2019-01-10 15:00:00', 0, 10, 50, 2000, 0),
(3, '2019-01-10 13:00:00', '2019-01-10 15:00:00', 10, 10, 90, 28000, 0),
(4, '2019-01-10 19:00:00', '2019-01-11 07:00:00', 10, 10, 90, 28000, 0),
(5, '2019-01-11 04:00:00', '2019-01-11 07:00:00', 20, 10, 50, 5800, 0),
(6, '2019-01-11 08:00:00', '2019-01-11 11:00:00', 20, 10, 50, 6500, 0),
(7, '2019-04-15 05:00:00', '2019-04-15 18:00:00', 0, 0, 10, 25000, 0),
(8, '2019-04-15 07:00:00', '2019-04-15 21:00:00', 22, 20, 20, 19000, 0),
(9, '2019-04-15 12:00:00', '2019-04-16 06:00:00', 13, 18, 15, 22000, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo_aerolinea`
--

CREATE TABLE `vuelo_aerolinea` (
  `id_vuelo` int(11) NOT NULL,
  `id_aerolinea` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vuelo_aerolinea`
--

INSERT INTO `vuelo_aerolinea` (`id_vuelo`, `id_aerolinea`) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 1),
(5, 1),
(6, 1),
(7, 2),
(8, 2),
(9, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo_desde`
--

CREATE TABLE `vuelo_desde` (
  `id_vuelo` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vuelo_desde`
--

INSERT INTO `vuelo_desde` (`id_vuelo`, `id_ciudad`) VALUES
(1, 1),
(2, 1),
(3, 3),
(4, 3),
(5, 4),
(6, 4),
(7, 1),
(8, 1),
(9, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo_hasta`
--

CREATE TABLE `vuelo_hasta` (
  `id_vuelo` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vuelo_hasta`
--

INSERT INTO `vuelo_hasta` (`id_vuelo`, `id_ciudad`) VALUES
(1, 4),
(2, 3),
(3, 4),
(4, 4),
(5, 5),
(6, 5),
(7, 5),
(8, 5),
(9, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo_reserva_vuelo`
--

CREATE TABLE `vuelo_reserva_vuelo` (
  `id_reserva_vuelo` int(11) NOT NULL,
  `id_vuelo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aerolinea`
--
ALTER TABLE `aerolinea`
  ADD PRIMARY KEY (`id_aerolinea`);

--
-- Indices de la tabla `agencia`
--
ALTER TABLE `agencia`
  ADD PRIMARY KEY (`id_agencia`);

--
-- Indices de la tabla `agencia_ciudad`
--
ALTER TABLE `agencia_ciudad`
  ADD PRIMARY KEY (`id_agencia`);

--
-- Indices de la tabla `agencia_franquicia`
--
ALTER TABLE `agencia_franquicia`
  ADD PRIMARY KEY (`id_agencia`);

--
-- Indices de la tabla `auto`
--
ALTER TABLE `auto`
  ADD PRIMARY KEY (`id_auto`);

--
-- Indices de la tabla `auto_agencia`
--
ALTER TABLE `auto_agencia`
  ADD PRIMARY KEY (`id_auto`);

--
-- Indices de la tabla `auto_gama`
--
ALTER TABLE `auto_gama`
  ADD PRIMARY KEY (`id_auto`);

--
-- Indices de la tabla `auto_marca`
--
ALTER TABLE `auto_marca`
  ADD PRIMARY KEY (`id_auto`);

--
-- Indices de la tabla `auto_reserva`
--
ALTER TABLE `auto_reserva`
  ADD PRIMARY KEY (`id_reserva_auto`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id_ciudad`);

--
-- Indices de la tabla `ciudad_provincia`
--
ALTER TABLE `ciudad_provincia`
  ADD PRIMARY KEY (`id_ciudad`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id_configuracion`);

--
-- Indices de la tabla `franquicia`
--
ALTER TABLE `franquicia`
  ADD PRIMARY KEY (`id_franquicia`);

--
-- Indices de la tabla `gama`
--
ALTER TABLE `gama`
  ADD PRIMARY KEY (`id_gama`);

--
-- Indices de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD PRIMARY KEY (`id_habitacion`);

--
-- Indices de la tabla `habitacion_hotel`
--
ALTER TABLE `habitacion_hotel`
  ADD PRIMARY KEY (`id_habitacion`);

--
-- Indices de la tabla `habitacion_reserva_habitacion`
--
ALTER TABLE `habitacion_reserva_habitacion`
  ADD PRIMARY KEY (`id_reserva_habitacion`);

--
-- Indices de la tabla `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id_hotel`);

--
-- Indices de la tabla `hotel_ciudad`
--
ALTER TABLE `hotel_ciudad`
  ADD PRIMARY KEY (`id_hotel`);

--
-- Indices de la tabla `hotel_servicio`
--
ALTER TABLE `hotel_servicio`
  ADD PRIMARY KEY (`id_servicio`,`id_hotel`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`id_marca`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id_permiso`);

--
-- Indices de la tabla `permisos_role`
--
ALTER TABLE `permisos_role`
  ADD UNIQUE KEY `role` (`role`,`permiso`);

--
-- Indices de la tabla `permisos_usuario`
--
ALTER TABLE `permisos_usuario`
  ADD UNIQUE KEY `usuario` (`usuario`,`permiso`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`id_provincia`);

--
-- Indices de la tabla `provincia_pais`
--
ALTER TABLE `provincia_pais`
  ADD PRIMARY KEY (`id_provincia`);

--
-- Indices de la tabla `reserva_auto`
--
ALTER TABLE `reserva_auto`
  ADD PRIMARY KEY (`id_reserva_auto`);

--
-- Indices de la tabla `reserva_habitacion`
--
ALTER TABLE `reserva_habitacion`
  ADD PRIMARY KEY (`id_reserva_habitacion`);

--
-- Indices de la tabla `reserva_vuelo`
--
ALTER TABLE `reserva_vuelo`
  ADD PRIMARY KEY (`id_reserva_vuelo`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id_servicio`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario_reserva_auto`
--
ALTER TABLE `usuario_reserva_auto`
  ADD PRIMARY KEY (`id_reserva_auto`);

--
-- Indices de la tabla `usuario_reserva_habitacion`
--
ALTER TABLE `usuario_reserva_habitacion`
  ADD PRIMARY KEY (`id_reserva_habitacion`);

--
-- Indices de la tabla `usuario_reserva_vuelo`
--
ALTER TABLE `usuario_reserva_vuelo`
  ADD PRIMARY KEY (`id_reserva_vuelo`);

--
-- Indices de la tabla `vuelo`
--
ALTER TABLE `vuelo`
  ADD PRIMARY KEY (`id_vuelo`);

--
-- Indices de la tabla `vuelo_aerolinea`
--
ALTER TABLE `vuelo_aerolinea`
  ADD PRIMARY KEY (`id_vuelo`);

--
-- Indices de la tabla `vuelo_desde`
--
ALTER TABLE `vuelo_desde`
  ADD PRIMARY KEY (`id_vuelo`);

--
-- Indices de la tabla `vuelo_hasta`
--
ALTER TABLE `vuelo_hasta`
  ADD PRIMARY KEY (`id_vuelo`);

--
-- Indices de la tabla `vuelo_reserva_vuelo`
--
ALTER TABLE `vuelo_reserva_vuelo`
  ADD PRIMARY KEY (`id_reserva_vuelo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aerolinea`
--
ALTER TABLE `aerolinea`
  MODIFY `id_aerolinea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `agencia`
--
ALTER TABLE `agencia`
  MODIFY `id_agencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `auto`
--
ALTER TABLE `auto`
  MODIFY `id_auto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `id_ciudad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id_configuracion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `franquicia`
--
ALTER TABLE `franquicia`
  MODIFY `id_franquicia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `gama`
--
ALTER TABLE `gama`
  MODIFY `id_gama` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  MODIFY `id_habitacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `hotel`
--
ALTER TABLE `hotel`
  MODIFY `id_hotel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `id_marca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id_permiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `provincia`
--
ALTER TABLE `provincia`
  MODIFY `id_provincia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `reserva_auto`
--
ALTER TABLE `reserva_auto`
  MODIFY `id_reserva_auto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `reserva_habitacion`
--
ALTER TABLE `reserva_habitacion`
  MODIFY `id_reserva_habitacion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reserva_vuelo`
--
ALTER TABLE `reserva_vuelo`
  MODIFY `id_reserva_vuelo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `id_servicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `vuelo`
--
ALTER TABLE `vuelo`
  MODIFY `id_vuelo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
