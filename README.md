ReservasYA

Base de datos en modo test:

Para correr la aplicacion en modo testing, y que use la base de datos llamada "reservasya-test" hay que poner la variable $modoTest en true que se encuentra en el archivo de configuracion "application/Config.php"

-----------------------------------------------------------------------------------------------

Login:

* Cuando un usuario se loguea quedan disponibles las siguientes variables twig para usar en cualquier vista:

{{nombre_de_usuario}}
{{id_usuario}}
{{admin}}
{{comercial}}
{{cliente}}


Registro de usuario cliente:
* Se valida que no exista mail ni nombre de usuario en la base de datos.

----------------------------------------------------------------------------------------------------------
* Desde cualquier vista se puede acceder a las variables de configuracion del sistema, por ej 
{{config.pesos_por_puntos}}
Ademas estan en $_SESSION['config'] para acceder desde los controladoress

-------------------------------------------------------------------------------------------------------
Estados por los que va a pasar una transaccion (es importante que esten "tal cual" representados en la base de datos) para que toda la logica de la aplicacion funcione correctamente:

en carrito - esperando consumir - consumido - cancelado

---------------------------------------------------------------------------------------------------

Para las autorizaciones hay un metodo en la clase Controller que se llama autorizar y recibe como parametro los valores 'admin' 'comercial' ó 'cliente'
La idea es que por ejemplo dentro del metodo 
public function gestionarAgenciaView(){
       
        $this->autorizar('comercial'); //le paso con string el rol que quiero verificar.

lo primero que hago es llamar al autorizar para que verifique si tengo acceso autorizado.