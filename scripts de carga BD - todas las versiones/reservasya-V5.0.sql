-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-10-2018 a las 00:06:13
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `reservasya`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aerolinea`
--

CREATE TABLE `aerolinea` (
  `id_aerolinea` int(11) NOT NULL,
  `aerolinea` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aerolinea`
--

INSERT INTO `aerolinea` (`id_aerolinea`, `aerolinea`) VALUES
(1, 'Aerolineas Argentinas'),
(2, 'AlItalia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agencia`
--

CREATE TABLE `agencia` (
  `id_agencia` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `franquicia` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `agencia`
--

INSERT INTO `agencia` (`id_agencia`, `nombre`, `franquicia`) VALUES
(1, 'RentACar', ''),
(2, 'Ivis', ''),
(3, 'AlquiAuto', ''),
(4, 'sdfdgf', ''),
(5, 'ldsncv', ''),
(6, 'MDPAutos', ''),
(7, 'mdqautos', ''),
(8, 'lpautos', ''),
(9, 'lapautos', ''),
(10, 'Martin Gonzalez Agency', 'Ivis');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agencia_ciudad`
--

CREATE TABLE `agencia_ciudad` (
  `id_agencia` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `agencia_ciudad`
--

INSERT INTO `agencia_ciudad` (`id_agencia`, `id_ciudad`) VALUES
(1, 1),
(2, 2),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 2),
(8, 1),
(9, 1),
(10, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto`
--

CREATE TABLE `auto` (
  `id_auto` int(11) NOT NULL,
  `patente` varchar(10) NOT NULL,
  `autonomia` int(11) NOT NULL,
  `capacidad` int(11) NOT NULL,
  `eliminado` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto`
--

INSERT INTO `auto` (`id_auto`, `patente`, `autonomia`, `capacidad`, `eliminado`) VALUES
(1, 'AKJ987', 450, 5, 0),
(2, 'LOP099', 450, 5, 0),
(3, 'AKH265', 256, 8, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_agencia`
--

CREATE TABLE `auto_agencia` (
  `id_auto` int(11) NOT NULL,
  `id_agencia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto_agencia`
--

INSERT INTO `auto_agencia` (`id_auto`, `id_agencia`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_gama`
--

CREATE TABLE `auto_gama` (
  `id_auto` int(11) NOT NULL,
  `id_gama` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto_gama`
--

INSERT INTO `auto_gama` (`id_auto`, `id_gama`) VALUES
(1, 2),
(2, 2),
(3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_marca`
--

CREATE TABLE `auto_marca` (
  `id_auto` int(11) NOT NULL,
  `id_marca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto_marca`
--

INSERT INTO `auto_marca` (`id_auto`, `id_marca`) VALUES
(1, 2),
(2, 2),
(3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_reserva`
--

CREATE TABLE `auto_reserva` (
  `id_reserva_auto` int(11) NOT NULL,
  `id_auto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto_reserva`
--

INSERT INTO `auto_reserva` (`id_reserva_auto`, `id_auto`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id_ciudad` int(11) NOT NULL,
  `ciudad` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`id_ciudad`, `ciudad`) VALUES
(1, 'La Plata'),
(2, 'Mar del Plata');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad_provincia`
--

CREATE TABLE `ciudad_provincia` (
  `id_ciudad` int(11) NOT NULL,
  `id_provincia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad_provincia`
--

INSERT INTO `ciudad_provincia` (`id_ciudad`, `id_provincia`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id_configuracion` int(11) NOT NULL,
  `variable` varchar(50) NOT NULL,
  `valor` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id_configuracion`, `variable`, `valor`) VALUES
(1, 'pesos_por_puntos', '8'),
(2, 'puntos_por_pesos', '5'),
(3, 'max_gap', '5'),
(4, 'min_gap_aeropuerto', '3'),
(5, 'factor_primera', '20'),
(6, 'factor_business', '40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gama`
--

CREATE TABLE `gama` (
  `id_gama` int(11) NOT NULL,
  `gama` varchar(10) NOT NULL,
  `precio_por_dia` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gama`
--

INSERT INTO `gama` (`id_gama`, `gama`, `precio_por_dia`) VALUES
(1, 'alta', 1850),
(2, 'media', 1700),
(3, 'baja', 1500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion`
--

CREATE TABLE `habitacion` (
  `id_habitacion` int(11) NOT NULL,
  `capacidad` int(11) NOT NULL,
  `eliminado` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habitacion`
--

INSERT INTO `habitacion` (`id_habitacion`, `capacidad`, `eliminado`) VALUES
(1, 5, 1),
(2, 5, 1),
(3, 3, 0),
(4, 3, 1),
(5, 5, 0),
(6, 10, 0),
(7, 5, 0),
(8, 5, 0),
(9, 5, 0),
(10, 5, 0),
(11, 5, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion_hotel`
--

CREATE TABLE `habitacion_hotel` (
  `id_habitacion` int(11) NOT NULL,
  `id_hotel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habitacion_hotel`
--

INSERT INTO `habitacion_hotel` (`id_habitacion`, `id_hotel`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 2),
(7, 2),
(8, 2),
(9, 4),
(10, 4),
(11, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion_reserva_habitacion`
--

CREATE TABLE `habitacion_reserva_habitacion` (
  `id_reserva_habitacion` int(11) NOT NULL,
  `id_habitacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habitacion_reserva_habitacion`
--

INSERT INTO `habitacion_reserva_habitacion` (`id_reserva_habitacion`, `id_habitacion`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel`
--

CREATE TABLE `hotel` (
  `id_hotel` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `estrellas` int(11) NOT NULL,
  `puntos` int(11) NOT NULL,
  `precio_por_persona` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hotel`
--

INSERT INTO `hotel` (`id_hotel`, `nombre`, `estrellas`, `puntos`, `precio_por_persona`) VALUES
(1, 'Sudestada', 5, 0, 1275),
(2, 'Roma', 3, 0, 1275.89),
(3, 'Mendoza', 3, 0, 5478.58),
(4, 'Margarita', 3, 0, 158);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel_ciudad`
--

CREATE TABLE `hotel_ciudad` (
  `id_hotel` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hotel_ciudad`
--

INSERT INTO `hotel_ciudad` (`id_hotel`, `id_ciudad`) VALUES
(0, 2),
(1, 2),
(2, 1),
(3, 1),
(4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel_servicio`
--

CREATE TABLE `hotel_servicio` (
  `id_servicio` int(11) NOT NULL,
  `id_hotel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hotel_servicio`
--

INSERT INTO `hotel_servicio` (`id_servicio`, `id_hotel`) VALUES
(1, 0),
(1, 1),
(1, 2),
(1, 3),
(2, 0),
(2, 1),
(2, 3),
(2, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `id_marca` int(11) NOT NULL,
  `marca` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`id_marca`, `marca`) VALUES
(1, 'Volkswagen'),
(2, 'Peugeot');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id_pais` int(11) NOT NULL,
  `pais` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id_pais`, `pais`) VALUES
(1, 'Argentina');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id_permiso` int(11) NOT NULL,
  `permiso` varchar(100) NOT NULL,
  `key` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id_permiso`, `permiso`, `key`) VALUES
(1, 'Tareas de administracion', 'admin_access'),
(2, 'Agregar Posts', 'nuevo_post'),
(3, 'Editar Posts', 'editar_post'),
(4, 'Eliminar Posts', 'eliminar_post');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos_role`
--

CREATE TABLE `permisos_role` (
  `role` int(11) NOT NULL,
  `permiso` int(11) NOT NULL,
  `valor` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos_role`
--

INSERT INTO `permisos_role` (`role`, `permiso`, `valor`) VALUES
(1, 1, 1),
(1, 2, 1),
(1, 3, 1),
(1, 4, 1),
(2, 2, 1),
(2, 3, 1),
(2, 4, 1),
(3, 2, 1),
(3, 3, 1),
(4, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos_usuario`
--

CREATE TABLE `permisos_usuario` (
  `usuario` int(11) NOT NULL,
  `permiso` int(11) NOT NULL,
  `valor` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `id_provincia` int(11) NOT NULL,
  `provincia` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`id_provincia`, `provincia`) VALUES
(1, 'Buenos Aires');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia_pais`
--

CREATE TABLE `provincia_pais` (
  `id_provincia` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provincia_pais`
--

INSERT INTO `provincia_pais` (`id_provincia`, `id_pais`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_auto`
--

CREATE TABLE `reserva_auto` (
  `id_reserva_auto` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `precio_por_dia` double NOT NULL,
  `desde` datetime NOT NULL,
  `hasta` datetime NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva_auto`
--

INSERT INTO `reserva_auto` (`id_reserva_auto`, `estado`, `precio_por_dia`, `desde`, `hasta`, `fecha_creacion`) VALUES
(1, 'en carrito', 1250.4, '2018-10-02 00:00:00', '2018-10-09 00:00:00', '2018-10-27 02:32:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_habitacion`
--

CREATE TABLE `reserva_habitacion` (
  `id_reserva_habitacion` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `precio_por_persona` double NOT NULL,
  `desde` datetime NOT NULL,
  `hasta` datetime NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva_habitacion`
--

INSERT INTO `reserva_habitacion` (`id_reserva_habitacion`, `estado`, `precio_por_persona`, `desde`, `hasta`, `fecha_creacion`) VALUES
(1, 'esperando consumir', 150, '2018-10-03 00:00:00', '2018-10-10 00:00:00', '2018-10-27 03:34:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_vuelo`
--

CREATE TABLE `reserva_vuelo` (
  `id_reserva_vuelo` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `cantidad_economica` int(11) NOT NULL,
  `cantidadad_primera` int(11) NOT NULL,
  `cantidad_business` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva_vuelo`
--

INSERT INTO `reserva_vuelo` (`id_reserva_vuelo`, `estado`, `cantidad_economica`, `cantidadad_primera`, `cantidad_business`, `fecha_creacion`) VALUES
(1, 'en carrito', 0, 0, 3, '2018-10-26 18:28:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `id_servicio` int(11) NOT NULL,
  `tipo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`id_servicio`, `tipo`) VALUES
(1, 'Cancha de futbol'),
(2, 'Gimnasio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombre_de_usuario` varchar(100) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `contrasena` varchar(50) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `dni` varchar(8) DEFAULT NULL,
  `puntos` int(11) DEFAULT NULL,
  `nro_tarjeta` varchar(50) DEFAULT NULL,
  `pregunta_secreta` varchar(50) DEFAULT NULL,
  `respuesta_secreta` varchar(50) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT '0',
  `comercial` tinyint(1) DEFAULT '0',
  `cliente` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre_de_usuario`, `mail`, `contrasena`, `nombre`, `apellido`, `dni`, `puntos`, `nro_tarjeta`, `pregunta_secreta`, `respuesta_secreta`, `admin`, `comercial`, `cliente`) VALUES
(10, 'marcos', 'marcos@marcos.com', '123456', 'Marcos', 'Gomez', '12456898', NULL, NULL, 'lala?', 'lala', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(4) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` int(11) NOT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `codigo` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `usuario`, `pass`, `email`, `role`, `estado`, `fecha`, `codigo`) VALUES
(1, 'nombre1', 'admin', 'd1b254c9620425f582e27f0044be34bee087d8b4', 'admin@admin.adm', 1, 1, '0000-00-00 00:00:00', 1963007335),
(2, 'usuario1', 'usuario1', 'd1b254c9620425f582e27f0044be34bee087d8b4', 'usuario1@user.com', 2, 1, '2012-03-21 20:53:07', 1963007335),
(3, 'usuario2', 'usuario2', 'd1b254c9620425f582e27f0044be34bee087d8b4', 'usuario2@user.com', 3, 1, '2012-03-21 20:57:01', 1963007335);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_reserva_auto`
--

CREATE TABLE `usuario_reserva_auto` (
  `id_usuario` int(11) NOT NULL,
  `id_reserva_auto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_reserva_auto`
--

INSERT INTO `usuario_reserva_auto` (`id_usuario`, `id_reserva_auto`) VALUES
(10, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_reserva_habitacion`
--

CREATE TABLE `usuario_reserva_habitacion` (
  `id_reserva_habitacion` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_reserva_habitacion`
--

INSERT INTO `usuario_reserva_habitacion` (`id_reserva_habitacion`, `id_usuario`) VALUES
(1, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_reserva_vuelo`
--

CREATE TABLE `usuario_reserva_vuelo` (
  `id_reserva_vuelo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_reserva_vuelo`
--

INSERT INTO `usuario_reserva_vuelo` (`id_reserva_vuelo`, `id_usuario`) VALUES
(1, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo`
--

CREATE TABLE `vuelo` (
  `id_vuelo` int(11) NOT NULL,
  `salida` datetime NOT NULL,
  `llegada` datetime NOT NULL,
  `capacidad_primera` int(11) NOT NULL,
  `capacidad_business` int(11) NOT NULL,
  `capacidad_economica` int(11) NOT NULL,
  `precio` double NOT NULL,
  `eliminado` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vuelo`
--

INSERT INTO `vuelo` (`id_vuelo`, `salida`, `llegada`, `capacidad_primera`, `capacidad_business`, `capacidad_economica`, `precio`, `eliminado`) VALUES
(1, '2018-10-19 02:40:00', '2018-10-20 05:30:00', 30, 40, 20, 15400, 0),
(2, '2018-10-20 02:40:00', '2018-10-21 05:30:00', 30, 40, 20, 15400, 0),
(3, '2018-10-21 02:40:00', '2018-10-22 05:30:00', 30, 40, 20, 15400, 0),
(4, '2018-10-22 02:40:00', '2018-10-23 05:30:00', 30, 40, 20, 15400, 0),
(5, '2018-10-23 02:40:00', '2018-10-24 05:30:00', 30, 40, 20, 15400, 0),
(6, '2018-10-24 02:40:00', '2018-10-25 05:30:00', 30, 40, 20, 15400, 0),
(7, '2018-10-25 02:40:00', '2018-10-26 05:30:00', 30, 40, 20, 15400, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo_aerolinea`
--

CREATE TABLE `vuelo_aerolinea` (
  `id_vuelo` int(11) NOT NULL,
  `id_aerolinea` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vuelo_aerolinea`
--

INSERT INTO `vuelo_aerolinea` (`id_vuelo`, `id_aerolinea`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo_desde`
--

CREATE TABLE `vuelo_desde` (
  `id_vuelo` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vuelo_desde`
--

INSERT INTO `vuelo_desde` (`id_vuelo`, `id_ciudad`) VALUES
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo_hasta`
--

CREATE TABLE `vuelo_hasta` (
  `id_vuelo` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vuelo_hasta`
--

INSERT INTO `vuelo_hasta` (`id_vuelo`, `id_ciudad`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo_reserva_vuelo`
--

CREATE TABLE `vuelo_reserva_vuelo` (
  `id_reserva_vuelo` int(11) NOT NULL,
  `id_vuelo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vuelo_reserva_vuelo`
--

INSERT INTO `vuelo_reserva_vuelo` (`id_reserva_vuelo`, `id_vuelo`) VALUES
(1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aerolinea`
--
ALTER TABLE `aerolinea`
  ADD PRIMARY KEY (`id_aerolinea`);

--
-- Indices de la tabla `agencia`
--
ALTER TABLE `agencia`
  ADD PRIMARY KEY (`id_agencia`);

--
-- Indices de la tabla `agencia_ciudad`
--
ALTER TABLE `agencia_ciudad`
  ADD PRIMARY KEY (`id_agencia`);

--
-- Indices de la tabla `auto`
--
ALTER TABLE `auto`
  ADD PRIMARY KEY (`id_auto`);

--
-- Indices de la tabla `auto_agencia`
--
ALTER TABLE `auto_agencia`
  ADD PRIMARY KEY (`id_auto`);

--
-- Indices de la tabla `auto_gama`
--
ALTER TABLE `auto_gama`
  ADD PRIMARY KEY (`id_auto`);

--
-- Indices de la tabla `auto_marca`
--
ALTER TABLE `auto_marca`
  ADD PRIMARY KEY (`id_auto`);

--
-- Indices de la tabla `auto_reserva`
--
ALTER TABLE `auto_reserva`
  ADD PRIMARY KEY (`id_reserva_auto`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id_ciudad`);

--
-- Indices de la tabla `ciudad_provincia`
--
ALTER TABLE `ciudad_provincia`
  ADD PRIMARY KEY (`id_ciudad`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id_configuracion`);

--
-- Indices de la tabla `gama`
--
ALTER TABLE `gama`
  ADD PRIMARY KEY (`id_gama`);

--
-- Indices de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD PRIMARY KEY (`id_habitacion`);

--
-- Indices de la tabla `habitacion_hotel`
--
ALTER TABLE `habitacion_hotel`
  ADD PRIMARY KEY (`id_habitacion`);

--
-- Indices de la tabla `habitacion_reserva_habitacion`
--
ALTER TABLE `habitacion_reserva_habitacion`
  ADD PRIMARY KEY (`id_reserva_habitacion`);

--
-- Indices de la tabla `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id_hotel`);

--
-- Indices de la tabla `hotel_ciudad`
--
ALTER TABLE `hotel_ciudad`
  ADD PRIMARY KEY (`id_hotel`);

--
-- Indices de la tabla `hotel_servicio`
--
ALTER TABLE `hotel_servicio`
  ADD PRIMARY KEY (`id_servicio`,`id_hotel`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`id_marca`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id_permiso`);

--
-- Indices de la tabla `permisos_role`
--
ALTER TABLE `permisos_role`
  ADD UNIQUE KEY `role` (`role`,`permiso`);

--
-- Indices de la tabla `permisos_usuario`
--
ALTER TABLE `permisos_usuario`
  ADD UNIQUE KEY `usuario` (`usuario`,`permiso`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`id_provincia`);

--
-- Indices de la tabla `provincia_pais`
--
ALTER TABLE `provincia_pais`
  ADD PRIMARY KEY (`id_provincia`);

--
-- Indices de la tabla `reserva_auto`
--
ALTER TABLE `reserva_auto`
  ADD PRIMARY KEY (`id_reserva_auto`);

--
-- Indices de la tabla `reserva_habitacion`
--
ALTER TABLE `reserva_habitacion`
  ADD PRIMARY KEY (`id_reserva_habitacion`);

--
-- Indices de la tabla `reserva_vuelo`
--
ALTER TABLE `reserva_vuelo`
  ADD PRIMARY KEY (`id_reserva_vuelo`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id_servicio`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario_reserva_auto`
--
ALTER TABLE `usuario_reserva_auto`
  ADD PRIMARY KEY (`id_reserva_auto`);

--
-- Indices de la tabla `usuario_reserva_habitacion`
--
ALTER TABLE `usuario_reserva_habitacion`
  ADD PRIMARY KEY (`id_reserva_habitacion`);

--
-- Indices de la tabla `usuario_reserva_vuelo`
--
ALTER TABLE `usuario_reserva_vuelo`
  ADD PRIMARY KEY (`id_reserva_vuelo`);

--
-- Indices de la tabla `vuelo`
--
ALTER TABLE `vuelo`
  ADD PRIMARY KEY (`id_vuelo`);

--
-- Indices de la tabla `vuelo_aerolinea`
--
ALTER TABLE `vuelo_aerolinea`
  ADD PRIMARY KEY (`id_vuelo`);

--
-- Indices de la tabla `vuelo_desde`
--
ALTER TABLE `vuelo_desde`
  ADD PRIMARY KEY (`id_vuelo`);

--
-- Indices de la tabla `vuelo_hasta`
--
ALTER TABLE `vuelo_hasta`
  ADD PRIMARY KEY (`id_vuelo`);

--
-- Indices de la tabla `vuelo_reserva_vuelo`
--
ALTER TABLE `vuelo_reserva_vuelo`
  ADD PRIMARY KEY (`id_reserva_vuelo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `aerolinea`
--
ALTER TABLE `aerolinea`
  MODIFY `id_aerolinea` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `agencia`
--
ALTER TABLE `agencia`
  MODIFY `id_agencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `auto`
--
ALTER TABLE `auto`
  MODIFY `id_auto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  MODIFY `id_ciudad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id_configuracion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `gama`
--
ALTER TABLE `gama`
  MODIFY `id_gama` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  MODIFY `id_habitacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `hotel`
--
ALTER TABLE `hotel`
  MODIFY `id_hotel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `id_marca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `pais`
--
ALTER TABLE `pais`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `permisos`
--
ALTER TABLE `permisos`
  MODIFY `id_permiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `provincia`
--
ALTER TABLE `provincia`
  MODIFY `id_provincia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `reserva_auto`
--
ALTER TABLE `reserva_auto`
  MODIFY `id_reserva_auto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `reserva_habitacion`
--
ALTER TABLE `reserva_habitacion`
  MODIFY `id_reserva_habitacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `reserva_vuelo`
--
ALTER TABLE `reserva_vuelo`
  MODIFY `id_reserva_vuelo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `id_servicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(4) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `vuelo`
--
ALTER TABLE `vuelo`
  MODIFY `id_vuelo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
