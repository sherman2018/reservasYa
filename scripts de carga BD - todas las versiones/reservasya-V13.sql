-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-11-2018 a las 21:37:37
-- Versión del servidor: 10.1.24-MariaDB
-- Versión de PHP: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `reservasya`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aerolinea`
--

CREATE TABLE `aerolinea` (
  `id_aerolinea` int(11) NOT NULL,
  `aerolinea` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `aerolinea`
--

INSERT INTO `aerolinea` (`id_aerolinea`, `aerolinea`) VALUES
(1, 'Aerolineas Argentinas'),
(2, 'AlItalia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agencia`
--

CREATE TABLE `agencia` (
  `id_agencia` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `agencia`
--

INSERT INTO `agencia` (`id_agencia`, `nombre`) VALUES
(1, 'RentACar'),
(2, 'Ivis'),
(3, 'AlquiAuto'),
(4, 'sdfdgf'),
(5, 'ldsncv'),
(6, 'MDPAutos'),
(7, 'Jose Fernandez Autos'),
(8, 'lpautos'),
(9, 'MardelAutos'),
(10, 'Martin Gonzalez Agency'),
(11, 'Prueba10'),
(12, 'PepeIvis'),
(13, 'MonteCarlo'),
(14, 'lalalkak'),
(15, 'tge'),
(16, 'cdba 1234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agencia_ciudad`
--

CREATE TABLE `agencia_ciudad` (
  `id_agencia` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `agencia_ciudad`
--

INSERT INTO `agencia_ciudad` (`id_agencia`, `id_ciudad`) VALUES
(0, 3),
(1, 1),
(2, 2),
(3, 3),
(4, 1),
(5, 3),
(6, 2),
(7, 1),
(8, 2),
(9, 2),
(10, 2),
(11, 3),
(12, 1),
(13, 2),
(14, 2),
(15, 1),
(16, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agencia_franquicia`
--

CREATE TABLE `agencia_franquicia` (
  `id_agencia` int(11) NOT NULL,
  `id_franquicia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `agencia_franquicia`
--

INSERT INTO `agencia_franquicia` (`id_agencia`, `id_franquicia`) VALUES
(0, 3),
(1, 1),
(2, 2),
(3, 1),
(4, 2),
(5, 3),
(6, 3),
(7, 2),
(8, 1),
(9, 3),
(10, 2),
(11, 3),
(12, 2),
(13, 3),
(14, 2),
(15, 1),
(16, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto`
--

CREATE TABLE `auto` (
  `id_auto` int(11) NOT NULL,
  `patente` varchar(10) NOT NULL,
  `autonomia` int(11) NOT NULL,
  `capacidad` int(11) NOT NULL,
  `eliminado` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto`
--

INSERT INTO `auto` (`id_auto`, `patente`, `autonomia`, `capacidad`, `eliminado`) VALUES
(1, 'AKJ987', 450, 5, 0),
(2, 'LOP099', 450, 5, 0),
(3, 'AKH265', 256, 8, 1),
(4, 'MKG987', 380, 5, 0),
(5, 'AA564CD', 500, 7, 0),
(6, 'GVO991', 380, 5, 0),
(7, '', 1635, 5, 1),
(8, '', 1635, 5, 1),
(9, '', 1635, 5, 1),
(10, '', 1635, 5, 1),
(11, '', 1635, 5, 0),
(12, '', 1635, 5, 0),
(13, '', 1635, 5, 0),
(14, '', 1635, 5, 0),
(15, '', 1635, 5, 0),
(16, '', 1635, 5, 0),
(17, '', 1356, 3, 1),
(18, '', 1356, 3, 0),
(19, '', 1356, 3, 0),
(20, '', 1356, 3, 0),
(21, '', 1356, 3, 0),
(22, '', 400, 2, 1),
(23, '', 400, 2, 0),
(24, '', 400, 2, 0),
(25, '', 125, 3, 1),
(26, '', 125, 3, 0),
(27, '', 2, 4, 1),
(28, '', 2, 4, 1),
(29, '', 2, 4, 0),
(30, '', 2, 4, 0),
(31, '', 2, 2, 0),
(32, '', 2, 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_agencia`
--

CREATE TABLE `auto_agencia` (
  `id_auto` int(11) NOT NULL,
  `id_agencia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto_agencia`
--

INSERT INTO `auto_agencia` (`id_auto`, `id_agencia`) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 7),
(5, 10),
(6, 3),
(7, 12),
(8, 12),
(9, 12),
(10, 12),
(11, 12),
(12, 12),
(13, 12),
(14, 12),
(15, 12),
(16, 12),
(17, 13),
(18, 13),
(19, 13),
(20, 13),
(21, 13),
(22, 14),
(23, 14),
(24, 14),
(25, 15),
(26, 15),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 16),
(32, 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_gama`
--

CREATE TABLE `auto_gama` (
  `id_auto` int(11) NOT NULL,
  `id_gama` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto_gama`
--

INSERT INTO `auto_gama` (`id_auto`, `id_gama`) VALUES
(1, 2),
(2, 2),
(3, 1),
(4, 2),
(5, 1),
(6, 3),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 1),
(23, 1),
(24, 1),
(25, 2),
(26, 2),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_marca`
--

CREATE TABLE `auto_marca` (
  `id_auto` int(11) NOT NULL,
  `id_marca` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto_marca`
--

INSERT INTO `auto_marca` (`id_auto`, `id_marca`) VALUES
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 1),
(6, 1),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(25, 2),
(26, 2),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auto_reserva`
--

CREATE TABLE `auto_reserva` (
  `id_reserva_auto` int(11) NOT NULL,
  `id_auto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auto_reserva`
--

INSERT INTO `auto_reserva` (`id_reserva_auto`, `id_auto`) VALUES
(0, 17),
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

CREATE TABLE `ciudad` (
  `id_ciudad` int(11) NOT NULL,
  `ciudad` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`id_ciudad`, `ciudad`) VALUES
(1, 'La Plata'),
(2, 'Mar del Plata'),
(3, 'Cordoba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad_provincia`
--

CREATE TABLE `ciudad_provincia` (
  `id_ciudad` int(11) NOT NULL,
  `id_provincia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ciudad_provincia`
--

INSERT INTO `ciudad_provincia` (`id_ciudad`, `id_provincia`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id_configuracion` int(11) NOT NULL,
  `variable` varchar(50) NOT NULL,
  `valor` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id_configuracion`, `variable`, `valor`) VALUES
(1, 'pesos_por_puntos', '8'),
(2, 'puntos_por_pesos', '10'),
(3, 'max_gap', '5'),
(4, 'min_gap_aeropuerto', '3'),
(5, 'factor_primera', '20'),
(6, 'factor_business', '40'),
(7, 'factor_devolucion_por_dia', '10'),
(8, 'factor_descuento_escala', '25'),
(9, 'items_por_pagina', '5');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `franquicia`
--

CREATE TABLE `franquicia` (
  `id_franquicia` int(11) NOT NULL,
  `franquicia` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `franquicia`
--

INSERT INTO `franquicia` (`id_franquicia`, `franquicia`) VALUES
(1, 'Lombardi'),
(2, 'Ivis'),
(3, 'Chichilo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gama`
--

CREATE TABLE `gama` (
  `id_gama` int(11) NOT NULL,
  `gama` varchar(10) NOT NULL,
  `precio_por_dia` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gama`
--

INSERT INTO `gama` (`id_gama`, `gama`, `precio_por_dia`) VALUES
(1, 'alta', 1850),
(2, 'media', 1700),
(3, 'baja', 1500);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion`
--

CREATE TABLE `habitacion` (
  `id_habitacion` int(11) NOT NULL,
  `capacidad` int(11) NOT NULL,
  `eliminado` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habitacion`
--

INSERT INTO `habitacion` (`id_habitacion`, `capacidad`, `eliminado`) VALUES
(3, 3, 0),
(4, 4, 0),
(5, 4, 0),
(6, 4, 0),
(7, 4, 0),
(8, 4, 0),
(9, 4, 0),
(10, 4, 0),
(11, 4, 0),
(12, 4, 0),
(13, 4, 0),
(14, 4, 1),
(15, 4, 1),
(16, 4, 1),
(17, 4, 0),
(18, 4, 0),
(19, 4, 0),
(20, 4, 0),
(21, 4, 0),
(22, 4, 0),
(23, 4, 0),
(24, 5, 0),
(25, 5, 0),
(26, 5, 0),
(27, 5, 0),
(28, 5, 0),
(29, 5, 0),
(30, 5, 0),
(31, 5, 0),
(32, 5, 0),
(33, 5, 0),
(34, 5, 1),
(35, 5, 1),
(36, 5, 1),
(37, 5, 0),
(38, 5, 0),
(39, 5, 0),
(40, 5, 0),
(41, 5, 1),
(42, 5, 1),
(43, 5, 1),
(44, 5, 1),
(45, 5, 1),
(46, 5, 1),
(47, 5, 0),
(48, 5, 0),
(49, 10, 1),
(50, 10, 1),
(51, 10, 1),
(52, 10, 1),
(53, 10, 0),
(54, 10, 0),
(55, 3, 0),
(56, 3, 0),
(57, 3, 0),
(58, 3, 0),
(59, 3, 0),
(60, 5, 0),
(61, 5, 0),
(62, 5, 0),
(63, 5, 0),
(64, 5, 0),
(65, 4, 0),
(66, 4, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion_hotel`
--

CREATE TABLE `habitacion_hotel` (
  `id_habitacion` int(11) NOT NULL,
  `id_hotel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habitacion_hotel`
--

INSERT INTO `habitacion_hotel` (`id_habitacion`, `id_hotel`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 2),
(7, 2),
(8, 2),
(9, 4),
(10, 4),
(11, 4),
(12, 4),
(13, 4),
(14, 5),
(15, 5),
(16, 5),
(17, 5),
(18, 5),
(19, 5),
(20, 5),
(21, 5),
(22, 5),
(23, 5),
(24, 6),
(25, 6),
(26, 6),
(27, 6),
(28, 6),
(29, 6),
(30, 6),
(31, 6),
(32, 6),
(33, 6),
(34, 7),
(35, 7),
(36, 7),
(37, 7),
(38, 7),
(39, 7),
(40, 7),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitacion_reserva_habitacion`
--

CREATE TABLE `habitacion_reserva_habitacion` (
  `id_reserva_habitacion` int(11) NOT NULL,
  `id_habitacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `habitacion_reserva_habitacion`
--

INSERT INTO `habitacion_reserva_habitacion` (`id_reserva_habitacion`, `id_habitacion`) VALUES
(1, 3),
(2, 3),
(3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel`
--

CREATE TABLE `hotel` (
  `id_hotel` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `estrellas` int(11) NOT NULL,
  `puntos` int(11) NOT NULL,
  `veces_puntuado` int(11) NOT NULL DEFAULT '0',
  `precio_por_persona` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hotel`
--

INSERT INTO `hotel` (`id_hotel`, `nombre`, `estrellas`, `puntos`, `veces_puntuado`, `precio_por_persona`) VALUES
(1, 'Sudestada', 5, 18, 5, 1275),
(2, 'Roma', 3, 0, 0, 500),
(3, 'Mendoza', 3, 0, 0, 5478.58),
(4, 'Margarita', 3, 0, 0, 158),
(5, 'Palazio', 4, 0, 0, 750),
(6, 'hl', 3, 0, 0, 154),
(7, 'werew', 3, 0, 0, 2556),
(8, 'aaaaaaaa', 1, 0, 0, 234);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel_ciudad`
--

CREATE TABLE `hotel_ciudad` (
  `id_hotel` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hotel_ciudad`
--

INSERT INTO `hotel_ciudad` (`id_hotel`, `id_ciudad`) VALUES
(0, 2),
(1, 2),
(2, 1),
(3, 1),
(4, 2),
(5, 2),
(6, 2),
(7, 3),
(8, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hotel_servicio`
--

CREATE TABLE `hotel_servicio` (
  `id_servicio` int(11) NOT NULL,
  `id_hotel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hotel_servicio`
--

INSERT INTO `hotel_servicio` (`id_servicio`, `id_hotel`) VALUES
(1, 0),
(1, 1),
(1, 2),
(1, 3),
(1, 5),
(1, 6),
(1, 8),
(2, 0),
(2, 1),
(2, 3),
(2, 4),
(2, 5),
(2, 7),
(3, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `id_marca` int(11) NOT NULL,
  `marca` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`id_marca`, `marca`) VALUES
(1, 'Volkswagen'),
(2, 'Peugeot');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pais`
--

CREATE TABLE `pais` (
  `id_pais` int(11) NOT NULL,
  `pais` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pais`
--

INSERT INTO `pais` (`id_pais`, `pais`) VALUES
(1, 'Argentina');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `id_permiso` int(11) NOT NULL,
  `permiso` varchar(100) NOT NULL,
  `key` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`id_permiso`, `permiso`, `key`) VALUES
(1, 'Tareas de administracion', 'admin_access'),
(2, 'Agregar Posts', 'nuevo_post'),
(3, 'Editar Posts', 'editar_post'),
(4, 'Eliminar Posts', 'eliminar_post');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos_role`
--

CREATE TABLE `permisos_role` (
  `role` int(11) NOT NULL,
  `permiso` int(11) NOT NULL,
  `valor` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos_role`
--

INSERT INTO `permisos_role` (`role`, `permiso`, `valor`) VALUES
(1, 1, 1),
(1, 2, 1),
(1, 3, 1),
(1, 4, 1),
(2, 2, 1),
(2, 3, 1),
(2, 4, 1),
(3, 2, 1),
(3, 3, 1),
(4, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos_usuario`
--

CREATE TABLE `permisos_usuario` (
  `usuario` int(11) NOT NULL,
  `permiso` int(11) NOT NULL,
  `valor` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `id_provincia` int(11) NOT NULL,
  `provincia` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`id_provincia`, `provincia`) VALUES
(1, 'Buenos Aires');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia_pais`
--

CREATE TABLE `provincia_pais` (
  `id_provincia` int(11) NOT NULL,
  `id_pais` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provincia_pais`
--

INSERT INTO `provincia_pais` (`id_provincia`, `id_pais`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_auto`
--

CREATE TABLE `reserva_auto` (
  `id_reserva_auto` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `precio_abonado` double NOT NULL,
  `puntos_a_acumular` int(11) NOT NULL DEFAULT '0',
  `desde` datetime NOT NULL,
  `hasta` datetime NOT NULL,
  `id_agencia_origen` int(11) NOT NULL,
  `id_agencia_destino` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `otroPais` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva_auto`
--

INSERT INTO `reserva_auto` (`id_reserva_auto`, `estado`, `precio_abonado`, `puntos_a_acumular`, `desde`, `hasta`, `id_agencia_origen`, `id_agencia_destino`, `fecha_creacion`, `otroPais`) VALUES
(0, 'en carrito', 1700, 0, '2018-11-27 00:00:00', '2018-11-28 00:00:00', 13, 5, '2018-11-25 17:36:21', 0),
(1, 'esperando consumir', 200, 217160, '2018-12-10 00:00:00', '2018-12-15 00:00:00', 1, 1, '2018-10-28 02:41:47', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_habitacion`
--

CREATE TABLE `reserva_habitacion` (
  `id_reserva_habitacion` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `precio_abonado` double NOT NULL,
  `puntos_a_acumular` int(11) NOT NULL DEFAULT '0',
  `desde` datetime NOT NULL,
  `hasta` datetime NOT NULL,
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `puntuado` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva_habitacion`
--

INSERT INTO `reserva_habitacion` (`id_reserva_habitacion`, `estado`, `precio_abonado`, `puntos_a_acumular`, `desde`, `hasta`, `fecha_creacion`, `puntuado`) VALUES
(1, 'esperando consumir', 200, 217160, '2018-12-02 00:00:00', '2018-12-09 00:00:00', '2018-10-28 02:44:25', 0),
(2, 'consumido', 3825, 217160, '2018-11-13 00:00:00', '2018-11-21 00:00:00', '2018-11-20 23:19:15', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva_vuelo`
--

CREATE TABLE `reserva_vuelo` (
  `id_reserva_vuelo` int(11) NOT NULL,
  `estado` varchar(50) NOT NULL,
  `cantidad_economica` int(11) NOT NULL,
  `cantidadad_primera` int(11) NOT NULL,
  `cantidad_business` int(11) NOT NULL,
  `precio_abonado` double NOT NULL,
  `puntos_a_acumular` int(11) NOT NULL DEFAULT '0',
  `fecha_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reserva_vuelo`
--

INSERT INTO `reserva_vuelo` (`id_reserva_vuelo`, `estado`, `cantidad_economica`, `cantidadad_primera`, `cantidad_business`, `precio_abonado`, `puntos_a_acumular`, `fecha_creacion`) VALUES
(1, 'cancelado', 0, 2, 0, 36960, 217160, '2018-10-28 02:26:01'),
(2, 'esperando consumir', 0, 0, 1, 16170, 217160, '2018-11-21 15:01:54'),
(3, 'esperando consumir', 0, 0, 1, 11025, 217160, '2018-11-21 15:01:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `id_servicio` int(11) NOT NULL,
  `tipo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`id_servicio`, `tipo`) VALUES
(1, 'Cancha de futbol'),
(2, 'Gimnasio'),
(3, 'Gym');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombre_de_usuario` varchar(100) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `contrasena` varchar(50) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `dni` varchar(8) DEFAULT NULL,
  `puntos` int(11) DEFAULT NULL,
  `nro_tarjeta` varchar(50) DEFAULT NULL,
  `pregunta_secreta` varchar(50) DEFAULT NULL,
  `respuesta_secreta` varchar(50) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT '0',
  `comercial` tinyint(1) DEFAULT '0',
  `cliente` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre_de_usuario`, `mail`, `contrasena`, `nombre`, `apellido`, `dni`, `puntos`, `nro_tarjeta`, `pregunta_secreta`, `respuesta_secreta`, `admin`, `comercial`, `cliente`) VALUES
(10, 'marcos', 'marcos@marcos.co', '123456', 'Marcos', 'Gomez', '12456898', 141293, NULL, 'lala', 'lal', 1, 1, 1),
(11, 'pepe', 'pepee@pepe', '123', 'prueba', 'pruebita', '12356988', NULL, NULL, 'jb?', 'lnl', 0, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(4) UNSIGNED NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` int(11) NOT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `codigo` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `usuario`, `pass`, `email`, `role`, `estado`, `fecha`, `codigo`) VALUES
(1, 'nombre1', 'admin', 'd1b254c9620425f582e27f0044be34bee087d8b4', 'admin@admin.adm', 1, 1, '0000-00-00 00:00:00', 1963007335),
(2, 'usuario1', 'usuario1', 'd1b254c9620425f582e27f0044be34bee087d8b4', 'usuario1@user.com', 2, 1, '2012-03-21 20:53:07', 1963007335),
(3, 'usuario2', 'usuario2', 'd1b254c9620425f582e27f0044be34bee087d8b4', 'usuario2@user.com', 3, 1, '2012-03-21 20:57:01', 1963007335);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_reserva_auto`
--

CREATE TABLE `usuario_reserva_auto` (
  `id_usuario` int(11) NOT NULL,
  `id_reserva_auto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_reserva_auto`
--

INSERT INTO `usuario_reserva_auto` (`id_usuario`, `id_reserva_auto`) VALUES
(10, 0),
(10, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_reserva_habitacion`
--

CREATE TABLE `usuario_reserva_habitacion` (
  `id_reserva_habitacion` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_reserva_habitacion`
--

INSERT INTO `usuario_reserva_habitacion` (`id_reserva_habitacion`, `id_usuario`) VALUES
(1, 10),
(2, 10),
(3, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_reserva_vuelo`
--

CREATE TABLE `usuario_reserva_vuelo` (
  `id_reserva_vuelo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_reserva_vuelo`
--

INSERT INTO `usuario_reserva_vuelo` (`id_reserva_vuelo`, `id_usuario`) VALUES
(1, 10),
(2, 10),
(3, 10),
(4, 10),
(5, 10),
(6, 10),
(7, 10),
(8, 10),
(9, 10),
(10, 10),
(11, 10),
(12, 10),
(13, 10),
(14, 10),
(15, 10),
(16, 10),
(17, 10),
(18, 10),
(19, 10),
(20, 10),
(21, 10),
(22, 10),
(23, 10),
(24, 10),
(25, 10),
(26, 10),
(27, 10),
(28, 10),
(29, 10),
(30, 10),
(31, 10),
(32, 10),
(33, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo`
--

CREATE TABLE `vuelo` (
  `id_vuelo` int(11) NOT NULL,
  `salida` datetime NOT NULL,
  `llegada` datetime NOT NULL,
  `capacidad_primera` int(11) NOT NULL,
  `capacidad_business` int(11) NOT NULL,
  `capacidad_economica` int(11) NOT NULL,
  `precio` double NOT NULL,
  `eliminado` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vuelo`
--

INSERT INTO `vuelo` (`id_vuelo`, `salida`, `llegada`, `capacidad_primera`, `capacidad_business`, `capacidad_economica`, `precio`, `eliminado`) VALUES
(1, '2018-11-29 02:40:00', '2018-11-29 06:10:00', 116, 39, 24, 15400, 0),
(2, '2018-11-29 08:40:00', '2018-11-29 11:30:00', 40, 38, 20, 10500, 0),
(3, '2018-11-29 08:40:00', '2018-11-29 11:30:00', 43, 30, 24, 10500, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo_aerolinea`
--

CREATE TABLE `vuelo_aerolinea` (
  `id_vuelo` int(11) NOT NULL,
  `id_aerolinea` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vuelo_aerolinea`
--

INSERT INTO `vuelo_aerolinea` (`id_vuelo`, `id_aerolinea`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo_desde`
--

CREATE TABLE `vuelo_desde` (
  `id_vuelo` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vuelo_desde`
--

INSERT INTO `vuelo_desde` (`id_vuelo`, `id_ciudad`) VALUES
(1, 1),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo_hasta`
--

CREATE TABLE `vuelo_hasta` (
  `id_vuelo` int(11) NOT NULL,
  `id_ciudad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vuelo_hasta`
--

INSERT INTO `vuelo_hasta` (`id_vuelo`, `id_ciudad`) VALUES
(1, 2),
(2, 3),
(3, 3),
(4, 1),
(5, 1),
(6, 1),
(7, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vuelo_reserva_vuelo`
--

CREATE TABLE `vuelo_reserva_vuelo` (
  `id_reserva_vuelo` int(11) NOT NULL,
  `id_vuelo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vuelo_reserva_vuelo`
--

INSERT INTO `vuelo_reserva_vuelo` (`id_reserva_vuelo`, `id_vuelo`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 2),
(9, 1),
(10, 3),
(11, 1),
(12, 2),
(13, 3),
(14, 1),
(15, 1),
(16, 2),
(17, 1),
(18, 1),
(19, 3),
(20, 1),
(21, 2),
(22, 1),
(23, 2),
(24, 1),
(25, 2),
(26, 1),
(27, 2),
(28, 2),
(29, 1),
(30, 2),
(31, 1),
(32, 1),
(33, 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aerolinea`
--
ALTER TABLE `aerolinea`
  ADD PRIMARY KEY (`id_aerolinea`);

--
-- Indices de la tabla `agencia`
--
ALTER TABLE `agencia`
  ADD PRIMARY KEY (`id_agencia`);

--
-- Indices de la tabla `agencia_ciudad`
--
ALTER TABLE `agencia_ciudad`
  ADD PRIMARY KEY (`id_agencia`);

--
-- Indices de la tabla `agencia_franquicia`
--
ALTER TABLE `agencia_franquicia`
  ADD PRIMARY KEY (`id_agencia`);

--
-- Indices de la tabla `auto`
--
ALTER TABLE `auto`
  ADD PRIMARY KEY (`id_auto`);

--
-- Indices de la tabla `auto_agencia`
--
ALTER TABLE `auto_agencia`
  ADD PRIMARY KEY (`id_auto`);

--
-- Indices de la tabla `auto_gama`
--
ALTER TABLE `auto_gama`
  ADD PRIMARY KEY (`id_auto`);

--
-- Indices de la tabla `auto_marca`
--
ALTER TABLE `auto_marca`
  ADD PRIMARY KEY (`id_auto`);

--
-- Indices de la tabla `auto_reserva`
--
ALTER TABLE `auto_reserva`
  ADD PRIMARY KEY (`id_reserva_auto`);

--
-- Indices de la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD PRIMARY KEY (`id_ciudad`);

--
-- Indices de la tabla `ciudad_provincia`
--
ALTER TABLE `ciudad_provincia`
  ADD PRIMARY KEY (`id_ciudad`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id_configuracion`);

--
-- Indices de la tabla `franquicia`
--
ALTER TABLE `franquicia`
  ADD PRIMARY KEY (`id_franquicia`);

--
-- Indices de la tabla `gama`
--
ALTER TABLE `gama`
  ADD PRIMARY KEY (`id_gama`);

--
-- Indices de la tabla `habitacion`
--
ALTER TABLE `habitacion`
  ADD PRIMARY KEY (`id_habitacion`);

--
-- Indices de la tabla `habitacion_hotel`
--
ALTER TABLE `habitacion_hotel`
  ADD PRIMARY KEY (`id_habitacion`);

--
-- Indices de la tabla `habitacion_reserva_habitacion`
--
ALTER TABLE `habitacion_reserva_habitacion`
  ADD PRIMARY KEY (`id_reserva_habitacion`);

--
-- Indices de la tabla `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id_hotel`);

--
-- Indices de la tabla `hotel_ciudad`
--
ALTER TABLE `hotel_ciudad`
  ADD PRIMARY KEY (`id_hotel`);

--
-- Indices de la tabla `hotel_servicio`
--
ALTER TABLE `hotel_servicio`
  ADD PRIMARY KEY (`id_servicio`,`id_hotel`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`id_marca`);

--
-- Indices de la tabla `pais`
--
ALTER TABLE `pais`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id_permiso`);

--
-- Indices de la tabla `permisos_role`
--
ALTER TABLE `permisos_role`
  ADD UNIQUE KEY `role` (`role`,`permiso`);

--
-- Indices de la tabla `permisos_usuario`
--
ALTER TABLE `permisos_usuario`
  ADD UNIQUE KEY `usuario` (`usuario`,`permiso`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`id_provincia`);

--
-- Indices de la tabla `provincia_pais`
--
ALTER TABLE `provincia_pais`
  ADD PRIMARY KEY (`id_provincia`);

--
-- Indices de la tabla `reserva_auto`
--
ALTER TABLE `reserva_auto`
  ADD PRIMARY KEY (`id_reserva_auto`);

--
-- Indices de la tabla `reserva_habitacion`
--
ALTER TABLE `reserva_habitacion`
  ADD PRIMARY KEY (`id_reserva_habitacion`);

--
-- Indices de la tabla `reserva_vuelo`
--
ALTER TABLE `reserva_vuelo`
  ADD PRIMARY KEY (`id_reserva_vuelo`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id_servicio`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario_reserva_auto`
--
ALTER TABLE `usuario_reserva_auto`
  ADD PRIMARY KEY (`id_reserva_auto`);

--
-- Indices de la tabla `usuario_reserva_habitacion`
--
ALTER TABLE `usuario_reserva_habitacion`
  ADD PRIMARY KEY (`id_reserva_habitacion`);

--
-- Indices de la tabla `usuario_reserva_vuelo`
--
ALTER TABLE `usuario_reserva_vuelo`
  ADD PRIMARY KEY (`id_reserva_vuelo`);

--
-- Indices de la tabla `vuelo`
--
ALTER TABLE `vuelo`
  ADD PRIMARY KEY (`id_vuelo`);

--
-- Indices de la tabla `vuelo_aerolinea`
--
ALTER TABLE `vuelo_aerolinea`
  ADD PRIMARY KEY (`id_vuelo`);

--
-- Indices de la tabla `vuelo_desde`
--
ALTER TABLE `vuelo_desde`
  ADD PRIMARY KEY (`id_vuelo`);

--
-- Indices de la tabla `vuelo_hasta`
--
ALTER TABLE `vuelo_hasta`
  ADD PRIMARY KEY (`id_vuelo`);

--
-- Indices de la tabla `vuelo_reserva_vuelo`
--
ALTER TABLE `vuelo_reserva_vuelo`
  ADD PRIMARY KEY (`id_reserva_vuelo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `agencia`
--
ALTER TABLE `agencia`
  MODIFY `id_agencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
