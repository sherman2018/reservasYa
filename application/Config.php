<?php


$modoTest=true;


define('BASE_URL', 'http://localhost/reservasYa/');
define('DEFAULT_CONTROLLER', 'index');
define('DEFAULT_LAYOUT', 'default');

define('APP_NAME', 'Mi Framework');
define('APP_SLOGAN', 'tutorial MVC y PHP...');
define('APP_COMPANY', '');
define('SESSION_TIME', 0);
define('HASH_KEY', '4f6a6d832be79');

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
$modoTest ? define('DB_NAME', 'reservasya-test') : define('DB_NAME', 'reservasya');
define('DB_CHAR', 'utf8');

?>