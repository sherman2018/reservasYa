<?php 
require_once(ROOT . DS. 'models'. DS.'autoModel.php');
require_once(ROOT . DS. 'models'. DS.'agenciaModel.php');
require_once(ROOT . DS. 'models'. DS.'marcaModel.php');
require_once(ROOT . DS. 'models'. DS.'gamaModel.php');
require_once(ROOT . DS. 'models'. DS.'ciudadModel.php');
require_once(ROOT . DS. 'models'. DS.'franquiciaModel.php');


class agenciaController extends Controller
{
    private $autoModel;
    public function __construct() {
        parent::__construct();
        $this->marcaModel = new marcaModel;
        $this->autoModel = new autoModel;
        $this->gamaModel = new gamaModel;
        $this->agenciaModel = new agenciaModel;
        $this->ciudadModel = new ciudadModel;
        $this->franquiciaModel = new franquiciaModel;
      
    }
    
    public function index()
    {
      

    }
    public function crear(){
        $this->autorizar('comercial');
        $nombre=$_POST['nombre'];
        $id_franquicia=$_POST['id_franquicia'];
        $id_ciudad= $_POST['ciudad'];
        $_REQUEST['agencia'] = $this->agenciaModel->crear($nombre, $id_franquicia, $id_ciudad);
        $this->_view->setParam("resultado",true);   
        $this->gestionarAgenciaView();
    }
   
    public function cargarAutos(){
        $this->autorizar('comercial');

        $cantidad=$_POST['cantidad'];
        $id_agencia= $_POST['agencia'];
        
        $capacidad=$_POST['capacidad'];
        $id_marca=$_POST['marca'];
        $id_gama=$_POST['gama'];
        $autonomia=$_POST['autonomia']; 
        
        

        for ($i=1; $i <= $cantidad ; $i++) { 
            $this->autoModel->crear($cantidad,$id_agencia, $capacidad, $id_marca, $id_gama, $autonomia);
        }
          $this->_view->setParam("resultado",true);
          $this->gestionarAgenciaView();



    }
    public function gestionarAgenciaView(){
        $this->autorizar('comercial');
   
        $marcas = $this->marcaModel->getMarcas();
        $this->_view->setParam("marcas",$marcas);
        $gamas = $this->gamaModel->getGamas();
        $this->_view->setParam("gamas",$gamas);
        $this->_view->setParam("agencia",explode("-", $_REQUEST['agencia'])[1]);
        $this->_view->setParam("id_agencia",explode("-", $_REQUEST['agencia'])[0]);
        $autos = $this->agenciaModel->getAutos(explode("-", $_REQUEST['agencia'])[0]);
        $this->_view->setParam("autos",$autos);
        $this->_view->renderizar('gestionar-agencia');
    }

    public function crearAgenciaView(){
        $this->autorizar('comercial');
        $ciudades = $this->ciudadModel->getCiudades();
        $franquicias = $this->franquiciaModel->getFranquicias();
        $this->_view->setParam("ciudades",$ciudades);
        $this->_view->setParam("franquicias",$franquicias);
        $this->_view->renderizar('crear-agencia');
    }

    public function eliminarAuto(){
        $this->autorizar('comercial');
        $id_agencia=$_POST['id_agencia'];
        $agencia=$_POST['agencia'];
        $gama=$_POST['gama'];
        $marca=$_POST['marca'];
        $capacidad=$_POST['capacidad'];
        $cantidad_a_eliminar=$_POST['cantidad_a_eliminar'];
        $this->autoModel->eliminar($id_agencia, $gama, $marca, $cantidad_a_eliminar, $capacidad);
        $_REQUEST['agencia']=$id_agencia." - ".$agencia;
        $this->gestionarAgenciaView();
    }
    
    public function buscarAuto() {
        
        $origen = $_POST['ciudadOrigenAuto'];
        $destino = $_POST['ciudadDestinoAuto'];
        $fechaInicio = $_POST['fechaInicioAuto'];
        $fechaFin = $_POST['fechaFinAuto'];
        $cantidadPersonas = $_POST['cantidadPersonasAuto'];
        $fechaDesde = date("d-m-Y", strtotime($fechaInicio));
        $fechaHasta = date("d-m-Y", strtotime($fechaFin));
        $fechaIn = new DateTime($fechaInicio);
        $fechaOut = new DateTime($fechaFin);
        $cant = $fechaOut->diff($fechaIn);
        $cantidadDias = $cant->days;
        
        $autos = $this->autoModel->buscarAuto($origen, $destino, $fechaInicio, $fechaFin, $cantidadPersonas);
        
        $_SESSION['primerFiltrado']['autos'] = $autos;
        $_SESSION['primerFiltrado']['segundo_filtrado_activo'] = 1;
        $_SESSION['primerFiltrado']['ciudadOrigenAuto'] = $origen;
        $_SESSION['primerFiltrado']['ciudadDestinoAuto'] = $destino;
        $_SESSION['primerFiltrado']['fechaInicioAuto'] = $fechaInicio;
        $_SESSION['primerFiltrado']['fechaFinAuto'] = $fechaFin;
        $_SESSION['primerFiltrado']['cantidadPersonasAuto'] = $cantidadPersonas;
        $_SESSION['primerFiltrado']['cantidadDias'] = $cantidadDias;
        $segundo_filtrado_activo = $_SESSION['primerFiltrado']['segundo_filtrado_activo'];
        
        $this->_view->setParam("segundo_filtrado_activo", $segundo_filtrado_activo);
        $this->_view->setParam("autos", $autos);
        $this->_view->setParam("fechaDesde", $fechaDesde);
        $this->_view->setParam("fechaHasta", $fechaHasta);
        $this->_view->setParam("cantidadDias", $cantidadDias);
        
        if(!empty($autos)) {
            $this->_view->renderizar('index');
        }
        else {
            $this->_view->renderizar('sinAutos');
        }
    }

    public function segundoFiltrado() {

        $segundo_filtrado_activo = $_SESSION['primerFiltrado']['segundo_filtrado_activo'] = 0;
        $origen = $_SESSION['primerFiltrado']['ciudadOrigenAuto'];
        $destino = $_SESSION['primerFiltrado']['ciudadDestinoAuto'];
        $fechaInicio = $_SESSION['primerFiltrado']['fechaInicioAuto'];
        $fechaFin = $_SESSION['primerFiltrado']['fechaFinAuto'];
        $cantidadPersonas = $_SESSION['primerFiltrado']['cantidadPersonasAuto'];
        $cantidadDias = $_SESSION['primerFiltrado']['cantidadDias'];
        $fechaDesde = date("d-m-Y", strtotime($fechaInicio));
        $fechaHasta = date("d-m-Y", strtotime($fechaFin));
                
        $autos = $this->autoModel->buscarAuto($origen, $destino, $fechaInicio, $fechaFin, $cantidadPersonas);

        $filterAutos = [];
        $filterAutosTemp = [];
        
          
        if(isset($_POST['marca'])){
            $marcaFiltrado = $this->getPostParam('marca');
            foreach ($autos as $key => $auto) {
                if ($auto['marca'] == $marcaFiltrado) {
                    $filterAutosTemp[] = $auto;
                }
            }
            $filterAutos = $filterAutosTemp;
        }else{
            $filterAutos = $autos;
        }

 

        if(isset($_POST['autonomiaMayor']) && ( $_POST['autonomiaMayor'] !== "")){
            $filterAutosTemp = [];
            $autonomiaFiltrado = $this->getPostParam('autonomiaMayor');
            foreach ($filterAutos as $key => $auto) {
                if ($auto['autonomia'] >= $autonomiaFiltrado) {
                    $filterAutosTemp[] = $auto;
                }
            }
            $filterAutos = $filterAutosTemp;
        }

        elseif(isset($_POST['autonomiaMenor']) && ( $_POST['autonomiaMenor'] !== "")){
            $filterAutosTemp = [];
            $autonomiaFiltrado = $this->getPostParam('autonomiaMenor');
            foreach ($filterAutos as $key => $auto) {
                if ($auto['autonomia'] <= $autonomiaFiltrado) {
                    $filterAutosTemp[] = $auto;
                }
            }
            $filterAutos = $filterAutosTemp;
        }




        
        if(isset($_POST['gama'])){
            $filterAutosTemp = [];
            $gamaFiltrado = $this->getPostParam('gama');
            foreach ($filterAutos as $key => $auto) {
                if ($auto['gama'] == $gamaFiltrado) {
                    $filterAutosTemp[] = $auto;
                }
            }
            $filterAutos = $filterAutosTemp;
        }
        
        if(isset($_POST['agencia'])){
            $filterAutosTemp = [];
            $agenciaFiltrado = $this->getPostParam('agencia');
            foreach ($filterAutos as $key => $auto) {
                if ($auto['nombre'] == $agenciaFiltrado) {
                    $filterAutosTemp[] = $auto;
                }
            }
            $filterAutos = $filterAutosTemp;
        }
        $this->_view->setParam("segundo_filtrado_activo", $segundo_filtrado_activo);
        $this->_view->setParam("autos", $filterAutos);
        $this->_view->setParam("fechaDesde", $fechaDesde);
        $this->_view->setParam("fechaHasta", $fechaHasta);
        $this->_view->setParam("cantidadDias", $cantidadDias);

       
        if (!empty($filterAutos)) {
            $this->_view->renderizar('index');
        } else {
            $this->_view->renderizar('sinAutos');
        }
    }
    
    public function quitarFiltrado(){
        $_SESSION['primerFiltrado']['segundo_filtrado_activo'] = 1;
        $segundo_filtrado_activo = $_SESSION['primerFiltrado']['segundo_filtrado_activo'];
        $autos = $_SESSION['primerFiltrado']['autos'];
        $cantidadDias = $_SESSION['primerFiltrado']['cantidadDias'];
        $fechaInicio = $_SESSION['primerFiltrado']['fechaInicioAuto'];
        $fechaFin = $_SESSION['primerFiltrado']['fechaFinAuto'];
        $fechaDesde = date("d-m-Y", strtotime($fechaInicio));
        $fechaHasta = date("d-m-Y", strtotime($fechaFin));
        
        $this->_view->setParam("segundo_filtrado_activo", $segundo_filtrado_activo);
        $this->_view->setParam("autos", $autos);
        $this->_view->setParam("fechaDesde", $fechaDesde);
        $this->_view->setParam("fechaHasta", $fechaHasta);
        $this->_view->setParam("cantidadDias", $cantidadDias);
        
        $this->_view->renderizar('index');
    }

    public function agregarCarrito(){
        $this->autorizar('cliente');
        $otroPais=0;
        $estado='en carrito';
        $precio_abonado=$_POST['precio_abonado'];
        $desde= date("Y-m-d",strtotime($_POST['fecha_desde']));
        $hasta= new DateTime($_POST['fecha_hasta']);
        $id_agencia_origen=$_POST['origenAgencia'];
        $id_agencia_destino=$_POST['destinoAgencia'];
        $id_usuario=$_POST['id_usuario'];
        $id_auto=$_POST['id_auto'];
        $origenPais=$_POST['origenPais'];
        $destinoPais=$_POST['destinoPais'];
        if($origenPais != $destinoPais){
            $hasta->modify('+1 day');
            $otroPais=1;
        }
        $fecha_hasta= $hasta->format("Y-m-d");
        $this->autoModel->agregarCarrito($estado, $precio_abonado, $desde, $fecha_hasta, $id_agencia_origen, $id_agencia_destino, $otroPais, $id_usuario, $id_auto);
        header('Location:../carrito');
    }
}

?>