<?php
require_once(ROOT . DS. 'models'. DS.'usuarioModel.php');
class transaccionesController extends Controller
{
    
    public function __construct() {
        parent::__construct();
        $this->usuarioModel = new usuarioModel;
    }
    
    public function index()
    {
        $this->autorizar('cliente');
        $id_usuario = $_SESSION['id_usuario'];
        $vuelos = $this->usuarioModel->obtenerVuelos($id_usuario);
        $this->_view->setParam("reservasVuelos",$vuelos);
        $autos = $this->usuarioModel->obtenerAutos($id_usuario);
        $this->_view->setParam("reservasAutos",$autos);
        $habitaciones = $this->usuarioModel->obtenerHabitaciones($id_usuario);
        $this->_view->setParam("reservasHabitaciones",$habitaciones);
        $this->_view->renderizar('index');
        
    }
}
?>