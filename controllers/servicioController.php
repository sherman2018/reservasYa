<?php

require_once(ROOT . DS. 'models'. DS.'servicioModel.php');
require_once(ROOT . DS. 'models'. DS.'agenciaModel.php');
require_once(ROOT . DS. 'models'. DS.'hotelModel.php');

class servicioController extends Controller
{
    private $servicioModel;
    public function __construct() {
        parent::__construct();
        $this->servicioModel = new servicioModel;
        $this->agenciaModel = new agenciaModel;
        $this->hotelModel = new hotelModel;
       
    }
    
    public function index()
    {
        $this->autorizar('comercial');
    	$agencias = $this->agenciaModel->getAgencias();
        $this->_view->setParam("agencias",$agencias);
        $hoteles = $this->hotelModel->getHoteles();
        $this->_view->setParam("hoteles",$hoteles);
        $this->_view->renderizar('index');
    }

    public function limpiarCarrito(){
        $this->autorizar('admin');
        $this->servicioModel->limpiarCarrito();
        $this->_view->renderizar('limpiezaOk');
    }
    
    
}

?>