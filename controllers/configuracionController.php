<?php

require_once(ROOT . DS. 'models'. DS.'configuracionModel.php');
class configuracionController extends Controller
{
    private $configuracionModel;
    public function __construct() {
        parent::__construct();
        $this->configuracionModel = new configuracionModel;
    }
    
    public function index()

    {
        $this->autorizar('admin');
        $this->_view->setParam('datos',$this->configuracionModel->getParametros());
        $this->_view->renderizar('index', 'inicio');
    }
    public function update(){
        $this->autorizar('admin');
        foreach ($_POST as $variable => $valor) {
            $this->configuracionModel->modificarParametro($variable, $valor);
        }
        header('Location:../configuracion');
   
    }
    public function getConfiguraciones(){
        
        return $this->configuracionModel->getConfiguraciones();
    }
    
}

?>