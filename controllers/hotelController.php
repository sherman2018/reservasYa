<?php 

require_once(ROOT . DS. 'models'. DS.'hotelModel.php');
require_once(ROOT . DS. 'models'. DS.'servicioModel.php');
require_once(ROOT . DS. 'models'. DS.'ciudadModel.php');


class hotelController extends Controller
{
    
    public function __construct() {
        parent::__construct();
        $this->hotelModel = new hotelModel;
        $this->ciudadModel = new ciudadModel;
        $this->servicioModel = new servicioModel;
      
    }
    public function index()
    {
        
      

    }
    public function crear()
    {
        $this->autorizar('comercial');
        $nombre=$_POST['nombre'];
        $id_ciudad=$_POST['ciudad'];
        $estrellas=$_POST['estrellas'];
        $precio=$_POST['precio'];
        $servicios=[];
        if (isset($_POST['servicios'])){
            $servicios=$_POST['servicios'];
        }

        $_POST['hotel'] = $this->hotelModel->crear($nombre, $id_ciudad, $estrellas, $precio, $servicios);
        $this->_view->setParam("resultado",true);


        $this->gestionarhotelView();
      

    }
    public function crearHotelView(){
        $this->autorizar('comercial');
        $ciudades = $this->ciudadModel->getCiudades();
        $this->_view->setParam("ciudades",$ciudades);
        $servicios = $this->hotelModel->getTodosLosServicios();
        $this->_view->setParam("servicios",$servicios);
        $this->_view->renderizar('crear-hotel');
    }
   
    
    public function gestionarhotelView(){
        $this->autorizar('comercial');
        $this->_view->setParam("hotel",explode("-", $_POST['hotel'])[1]);
        $this->_view->setParam("id_hotel",explode("-", $_POST['hotel'])[0]);
        $ciudades = $this->ciudadModel->getCiudades();
        $this->_view->setParam("ciudades",$ciudades);
        $habitaciones = $this->hotelModel->getHabitaciones(explode("-", $_POST['hotel'])[0]);
        $this->_view->setParam("habitaciones",$habitaciones);
        $this->_view->renderizar('gestionar-hotel');
    }

    public function recibirPuntuacion(){
        $this->autorizar('cliente');
        $puntaje=$_POST['puntaje'];
        $id_hotel=$_POST['id_hotel'];
        $id_reserva_habitacion=$_POST['id_reserva_habitacion'];
        $this->hotelModel->puntuar($id_hotel,$puntaje);
        $this->servicioModel->setReservaHabiacionPuntuada($id_reserva_habitacion);
        header("Location: ../transacciones");
    }
   
}

?>