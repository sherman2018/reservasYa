<?php 
require_once(ROOT . DS. 'models'. DS.'hotelModel.php');
require_once(ROOT . DS. 'models'. DS.'habitacionModel.php');


class habitacionController extends Controller
{
    private $habitacionModel;
    public function __construct() {
        parent::__construct();
        $this->habitacionModel = new habitacionModel;      
        $this->hotelModel = new hotelModel;    
    }
    
    public function index()
    {
          

    }
    public function createHabitacionView(){
        $this->autorizar('comercial');
               
        $this->_view->setParam("id_hotel",$_REQUEST['id_hotel']);
        $this->_view->setParam("hotel",$_REQUEST['nombreHotel']);
        $habitaciones = $this->hotelModel->getHabitaciones($_REQUEST['id_hotel']);
        $this->_view->setParam("habitaciones",$habitaciones);
        $this->_view->renderizar('../hotel/gestionar-hotel');
    }
    public function create(){
        $this->autorizar('comercial');

        $cantidad=$_POST['cantidad'];
        $id_hotel=$_POST['id_hotel'];
        $nombreHotel=$_POST['nombreHotel'];
        $capacidad=$_POST['capacidad'];
        

        for ($i=1; $i <= $cantidad ; $i++) { 
            $this->habitacionModel->crear($id_hotel, $capacidad);
        }
          $this->_view->setParam("resultado",true);
          $this->createHabitacionView();



    }

    public function agregarCarrito(){
        $this->autorizar('cliente');
        $id_habitacion=$_POST['id_habitacion'];
        $cantidad=$_POST['cantidad'];
        $fechaDesde=date("Y-m-d", strtotime($_POST['fecha_desde']));
        $fechaHasta=date("Y-m-d", strtotime($_POST['fecha_hasta']));
        $precio_persona=$_POST['precio_persona'];
        $id_usuario=$_POST['id_usuario'];
        $estado='en carrito';
         $this->habitacionModel->agregarCarrito($estado, $precio_persona, $fechaDesde, $fechaHasta, $id_habitacion, $id_usuario, $cantidad);
         
         header('Location:../carrito');
    }
    
    public function eliminar(){
        $this->autorizar('comercial');
        $id_hotel=$_POST['id_hotel'];
        $cantidad_a_eliminar=$_POST['cantidad'];
        $capacidad =$_POST['capacidad'];
        $this->habitacionModel->eliminar($id_hotel, $cantidad_a_eliminar, $capacidad);
        $this->createHabitacionView();
    }
    
    public function buscarHabitacion() {
        
        $ciudad = $_POST['ciudadHotel'];
        $desde = $_POST['fechaInicioHotel'];
        $hasta = $_POST['fechaFinHotel'];
        $cantidad = $_POST['cantidadPersonasHotel'];
        $fechaDesde = date("d-m-Y", strtotime($desde));
        $fechaHasta = date("d-m-Y", strtotime($hasta));
        $fechaIn = new DateTime($desde);
        $fechaOut = new DateTime($hasta);
        $cant = $fechaOut->diff($fechaIn);
        $cantidadDias = $cant->days;
        
        $habitaciones = $this->habitacionModel->buscarHabitacion($ciudad, $desde, $hasta, $cantidad);
        
        $_SESSION['primerFiltrado']['segundo_filtrado_activo'] = 1;
        $_SESSION['primerFiltrado']['habitaciones'] = $habitaciones;
        $_SESSION['primerFiltrado']['ciudadHotel'] = $ciudad;
        $_SESSION['primerFiltrado']['fechaInicioHotel'] = $desde;
        $_SESSION['primerFiltrado']['fechaFinHotel'] = $hasta;
        $_SESSION['primerFiltrado']['cantidadPersonasHotel'] = $cantidad;
        $segundo_filtrado_activo = $_SESSION['primerFiltrado']['segundo_filtrado_activo'];

        $serviciosDeHotel = array();
        foreach ($habitaciones as $habitacion) {
            $id_hotel = $habitacion['id_hotel'];
            $servicios=$this->hotelModel->obtenerServicios($id_hotel);
            $serviciosDeHotel[$id_hotel]=$servicios;
        }
        
        $_SESSION['primerFiltrado']['serviciosDeHotel'] = $serviciosDeHotel;
        
        $this->_view->setParam("serviciosDeHotel", $serviciosDeHotel);
        $this->_view->setParam("segundo_filtrado_activo", $segundo_filtrado_activo);
        $this->_view->setParam("habitaciones", $habitaciones);
        $this->_view->setParam("cantidad", $cantidad);
        $this->_view->setParam("fechaDesde", $fechaDesde);
        $this->_view->setParam("fechaHasta", $fechaHasta);
        $this->_view->setParam("cantidadDias", $cantidadDias);
        if(!empty($habitaciones)) {
            $this->_view->renderizar('index');
        }
        else {
            $this->_view->renderizar('sinHabitaciones');
        }
    }
    
    public function segundoFiltrado() {
        $segundo_filtrado_activo = $_SESSION['primerFiltrado']['segundo_filtrado_activo'] = 0;
        $ciudad = $_SESSION['primerFiltrado']['ciudadHotel'];
        $desde = $_SESSION['primerFiltrado']['fechaInicioHotel'];
        $hasta = $_SESSION['primerFiltrado']['fechaFinHotel'];
        $cantidad = $_SESSION['primerFiltrado']['cantidadPersonasHotel'];
        $fechaDesde = date("d-m-Y", strtotime($desde));
        $fechaHasta = date("d-m-Y", strtotime($hasta));
        $fechaIn = new DateTime($desde);
        $fechaOut = new DateTime($hasta);
        $cant = $fechaOut->diff($fechaIn);
        $cantidadDias = $cant->days;
        if (isset($_POST['hotel'])){
            $hotelFiltrado = $_POST['hotel'];
        }
        if (isset($_POST['cant_estrellas'])){
            $cant_estrellas = $_POST['cant_estrellas'];
        }
        
        $habitaciones = $this->habitacionModel->buscarHabitacion($ciudad, $desde, $hasta, $cantidad);
        
        $filterHabitaciones = [];
        $filterHabitacionesTemp = [];
        if (isset($_POST['hotel'])){
            foreach ($habitaciones as $key=> $habitacion) {
                if ($habitacion['nombre'] == $hotelFiltrado) {
                    $filterHabitaciones[] = $habitacion;
                }
            }
        }else{
            $filterHabitaciones = $habitaciones;
        }
        if (isset($_POST['cant_estrellas'])){
            foreach ($filterHabitaciones as $key=> $habitacion) {
                if ($habitacion['estrellas'] == $cant_estrellas) {
                    $filterHabitacionesTemp[] = $habitacion;
                }
            }
            $filterHabitaciones = $filterHabitacionesTemp;
        }
        if (isset($_POST['servicio'])){
            $filterHabitacionesTemp = [];
            $serviciosDeHotel = [];
            foreach ($filterHabitaciones as $habitacion) {
                $id_hotel = $habitacion['id_hotel'];
                $servicios=$this->hotelModel->obtenerServicios($id_hotel);
                $serviciosDeHotel[$id_hotel]=$servicios;
                
            }
            foreach ($filterHabitaciones as $key=> $habitacion) {
                $encontrado = false;
                foreach ($serviciosDeHotel[$habitacion['id_hotel']] as $serv) {
                    foreach ($serv as $s) {
                        if ($s == $_POST['servicio']){
                        $encontrado = true;
                        }
                    }
                    
                }
                if ($encontrado) {
                    $filterHabitacionesTemp[] = $habitacion;
                }
            }
            $filterHabitaciones = $filterHabitacionesTemp;
        }
        
        $serviciosDeHotel = array();
        foreach ($habitaciones as $habitacion) {
            $id_hotel = $habitacion['id_hotel'];
            $servicios=$this->hotelModel->obtenerServicios($id_hotel);
            $serviciosDeHotel[$id_hotel]=$servicios;
        }
        
        $this->_view->setParam("serviciosDeHotel", $serviciosDeHotel);
        $this->_view->setParam("segundo_filtrado_activo", $segundo_filtrado_activo);
        $this->_view->setParam("habitaciones", $filterHabitaciones);
        $this->_view->setParam("cantidad", $cantidad);
        $this->_view->setParam("fechaDesde", $fechaDesde);
        $this->_view->setParam("fechaHasta", $fechaHasta);
        $this->_view->setParam("cantidadDias", $cantidadDias);
        if(!empty($filterHabitaciones)) {
            $this->_view->renderizar('index');
        }
        else {
            $this->_view->renderizar('sinHabitaciones');
        }
    }
    
        public function quitarFiltrado(){
        $_SESSION['primerFiltrado']['segundo_filtrado_activo'] = 1;
        $segundo_filtrado_activo = $_SESSION['primerFiltrado']['segundo_filtrado_activo'];
        $habitaciones = $_SESSION['primerFiltrado']['habitaciones'];
        $ciudad = $_SESSION['primerFiltrado']['ciudadHotel'];
        $desde = $_SESSION['primerFiltrado']['fechaInicioHotel'];
        $hasta = $_SESSION['primerFiltrado']['fechaFinHotel'];
        $cantidad = $_SESSION['primerFiltrado']['cantidadPersonasHotel'];
        $serviciosDeHotel = $_SESSION['primerFiltrado']['serviciosDeHotel'];
        $fechaDesde = date("d-m-Y", strtotime($desde));
        $fechaHasta = date("d-m-Y", strtotime($hasta));
        $fechaIn = new DateTime($desde);
        $fechaOut = new DateTime($hasta);
        $cant = $fechaOut->diff($fechaIn);
        $cantidadDias = $cant->days;
                
        $this->_view->setParam("serviciosDeHotel", $serviciosDeHotel);
        $this->_view->setParam("segundo_filtrado_activo", $segundo_filtrado_activo);
        $this->_view->setParam("habitaciones", $habitaciones);
        $this->_view->setParam("cantidad", $cantidad);
        $this->_view->setParam("fechaDesde", $fechaDesde);
        $this->_view->setParam("fechaHasta", $fechaHasta);
        $this->_view->setParam("cantidadDias", $cantidadDias);
        
        $this->_view->renderizar('index');
    }

    public function consumir(){
        $id_reserva_habitacion = $_POST['nro_reserva_hab_consumir'];
        $this->habitacionModel->consumir($id_reserva_habitacion);
        header('Location:../');
    }
}



?>