<?php

require_once(ROOT . DS. 'models'. DS.'usuarioModel.php');
class usuarioController extends Controller
{
    private $usuarioModel;
    public function __construct() {
        parent::__construct();
        $this->usuarioModel = new usuarioModel;
    }
    
    public function index()
    {
        $this->_view->renderizar('index', 'inicio');
    }
    public function registro()
    {
        // Previo a renderizar seteo parametros
      /*  $params = array();
        $this->_view->setParam('parametro1', "valor 1");
        $this->_view->setParam('parametro2', "valor 2");
        $this->_view->setParam('parametro3', "valor 3");*/
        // Ultima linea
        $this->_view->renderizar('registro');
    }

    public function perfil($username){
        $usuario= $this->usuarioModel->getUsuario($username);
        $this->_view->setParam("usuario",$usuario);
        $this->_view->renderizar('perfil');
    }

    public function modificar(){
        $this->_view->setParam("datos",$_POST);
        $this->_view->renderizar('modificarPerfil');
    }

    public function login()
    {
    	$this->_view->renderizar('login');
    }
    public function logout()
    {
        session_destroy();
        header("Location: ../");
    }
    public function actualizarServiciosAConsumidos($id_usuario){
        //Tambien acumula puntos
        $this->usuarioModel->consumirServicios($id_usuario);

    }

    public function autenticar()
    {
        $nombre_de_usuario=$_POST['nombre_de_usuario'];
        $contrasena=$_POST['contrasena'];
        $id_usuario=$this->usuarioModel->autenticarUsuario($nombre_de_usuario, $contrasena);
        if ($id_usuario !== 0){
            $_SESSION['id_usuario'] = $id_usuario[0]['id_usuario'];
            $_SESSION['admin'] = $id_usuario[0]['admin'];
            $_SESSION['comercial'] = $id_usuario[0]['comercial'];
            $_SESSION['cliente'] = $id_usuario[0]['cliente'];
            $_SESSION['nombre_de_usuario'] = $id_usuario[0]['nombre_de_usuario'];
            $_SESSION['intentos_sesion']=0;
            //Tambien acumula puntos
            $this->actualizarServiciosAConsumidos($_SESSION['id_usuario']);
            //Tambien acumula puntos
            header("Location: ../");

        } else{
            error_reporting(E_ALL ^ E_NOTICE);
            $_SESSION['intentos_sesion'][$nombre_de_usuario]=$_SESSION['intentos_sesion'][$nombre_de_usuario]+1;
            if ($_SESSION['intentos_sesion'][$nombre_de_usuario] > 2) {
                $pregunta_secreta = $this->usuarioModel->preguntaSecreta($nombre_de_usuario);
                $this->_view->setParam('nombre_de_usuario', $nombre_de_usuario);
                $this->_view->setParam('pregunta_secreta', $pregunta_secreta);
                $this->_view->renderizar('responderPreguntaSecreta');
                session_destroy(); session_start();$_SESSION['intentos_sesion'][$nombre_de_usuario]=0;die;
            }
            $this->_view->setParam('errorAlLoguearse', true);
            $this->_view->renderizar('login');
        }
    }


    //Devuelde en string que rol tiene el usuario logueado.
    public function queRolTiene(){
            $queEs = ($_SESSION['admin']== 1) ? 'admin' : '';
            $queEs = ($_SESSION['comercial']== 1) ? 'comercial' : $queEs;
            $queEs = ($_SESSION['cliente']== 1) ? 'cliente' : $queEs;
            return $queEs;
    }


    private function noExisteUsuario($username, $mail){
        $encontrado=0;
        $encontrado = $this->usuarioModel->getUsuarioByUsername($username) ? ($encontrado + 1) : $encontrado;
        $encontrado = $this->usuarioModel->getUsuarioByMail($mail) ? ($encontrado + 1) : $encontrado;
        if ($encontrado > 0) return (false);
        return true;

    }
    public function create($esComercial=false){
        $nombre_de_usuario=$_POST['nombre_de_usuario'];
        $mail=$_POST['mail'];
        if ($this->noExisteUsuario($nombre_de_usuario,$mail)){
            $nombre=$_POST['nombre'];
            $apellido=$_POST['apellido'];

            isset($_POST['dni']) ? $dni=$_POST['dni'] : $dni="";
            isset($_POST['pregunta_secreta']) ? $pregunta_secreta=$_POST['pregunta_secreta'] : $pregunta_secreta="";
            isset($_POST['respuesta_secreta']) ? $respuesta_secreta=$_POST['respuesta_secreta'] : $respuesta_secreta="";

            
            $mail=$_POST['mail'];
            $contrasena=$_POST['contrasena'];
            ($esComercial) ? $esCliente=false : $esCliente=true;
            $this->usuarioModel->setUsuario($esComercial, $esCliente, $nombre, $apellido, $dni, $nombre_de_usuario, $mail, $contrasena, $pregunta_secreta, $respuesta_secreta);
            $this->_view->renderizar('registroCorrecto');
        } else{
            $this->_view->setParam('datos', $_POST);
            $this->_view->renderizar('registro');
        }
        
        
    }

    public function update(){
        $nombre_de_usuario=$_POST['nombre_de_usuario'];
        $mail=$_POST['mail'];
        $id_usuario=$_POST['id_usuario'];
        if ($this->validarUsuario($nombre_de_usuario, $mail, $id_usuario)){
            $nombre=$_POST['nombre'];
            $apellido=$_POST['apellido'];
            $dni=$_POST['dni'];
            $pregunta_secreta=$_POST['pregunta_secreta'];
            $respuesta_secreta=$_POST['respuesta_secreta'];
            $this->usuarioModel->update_Usuario($nombre_de_usuario, $mail, $nombre, $apellido, $dni, $pregunta_secreta, $respuesta_secreta, $id_usuario);
            $this->perfil($nombre_de_usuario);
        } else {
           $this->_view->setParam('error', $_POST);
           $this->_view->setParam('datos', $_POST);
           $this->_view->renderizar('modificarPerfil'); 
        }

    }

    public function validarUsuario($nombre_de_usuario, $mail, $id_usuario){
        $encontrado=0;
        $encontrado= $this->usuarioModel->validarUsuarioPorUsername($nombre_de_usuario, $id_usuario) ? ($encontrado + 1) : $encontrado;
        $encontrado= $this->usuarioModel->validarUsuarioPorMail($mail, $id_usuario) ? ($encontrado + 1) : $encontrado;
        if ($encontrado > 0) return (false);
        return true;
    }


    public function createComercialView(){
        $this->_view->renderizar('registroComercial');
        
        
    }

     public function createComercial(){
        $this->create(true);
        $this->_view->renderizar(ROOT);
        
    }

    public function cancelarReservaVuelo(){
        $id_reserva_vuelo=$_GET['id_reserva_vuelo'];
        $id_vuelo=$_GET['id_vuelo'];
        $cantidad_economica=$_GET['cantidad_economica'];
        $cantidad_primera=$_GET['cantidad_primera'];
        $cantidad_business=$_GET['cantidad_business'];
        $this->usuarioModel->cancelarReservaVuelo($id_reserva_vuelo,$id_vuelo,$cantidad_economica, $cantidad_primera, $cantidad_business);
        header("Location:../transacciones");


    }


    public function cancelarReservaAuto(){
        $id_reserva_auto=$_GET['id_reserva_auto'];
        $this->usuarioModel->cancelarReservaAuto($id_reserva_auto);
        header("Location:../transacciones");


    }

    public function cancelarReservaHabitacion(){
        $id_reserva_habitacion=$_GET['id_reserva_habitacion'];
        $this->usuarioModel->cancelarReservaHabitacion($id_reserva_habitacion);
        header("Location:../transacciones");


    }


    public function resetearContrasenaView(){
        $this->_view->renderizar('resetearContrasena');
    }

    public function resetearContrasena(){
        $nombre_de_usuario = $_POST['nombre_de_usuario'];
        $contrasena_nueva = $_POST['contrasena_nueva'];
        $this->usuarioModel->resetearContrasena($nombre_de_usuario, $contrasena_nueva);
        header("Location:../");
    }

    public function comprarTodoElCarrito(){
        $total_a_abonar = $_GET['total_a_abonar'];
        
        $nombre_de_usuario = $_SESSION['nombre_de_usuario'];

        $puntos = $this->usuarioModel->getPuntos($nombre_de_usuario);

        $this->_view->setParam("puntos",$puntos['puntos']);

        $this->_view->setParam("total_a_abonar",$total_a_abonar);

        $this->_view->renderizar('finalizarCompra');


    }

    public function pagarCompra(){
        error_reporting(E_ALL ^ E_NOTICE);
        $nombre_de_usuario = $_SESSION['nombre_de_usuario'];
        $idUsuario = $_SESSION['id_usuario'];
        $pesos_por_puntos= $_SESSION['config']['pesos_por_puntos'];
        $abona_con_tarjeta= $_POST['abona_con_tarjeta'];
        if (is_null($_POST['puntos_a_usar'])){
            $_POST['puntos_a_usar']=0;
        }





        //restar puntos del usuuario
        $this->usuarioModel->restarPuntos($nombre_de_usuario,$_POST['puntos_a_usar']);
        //pasar lo que tenga en carrito a esperando consumir
        $this->usuarioModel->pagarCompra($idUsuario, $pesos_por_puntos, $abona_con_tarjeta);
        $this->_view->renderizar('compraFinalizada');

        
       

    }
    public function restablecerContrasena(){
        
        $nombre_de_usuario=$_POST['nombre_de_usuario'];
        $respuesta=$_POST['respuesta'];
        $contrasena_nueva=$_POST['contrasena_nueva'];
        $exito=$this->usuarioModel->restablecerContrasenaConRespuesta($nombre_de_usuario, $respuesta, $contrasena_nueva);
        if ($exito){
            $this->_view->renderizar('exitoRestablecer');

        }
        $pregunta_secreta = $this->usuarioModel->preguntaSecreta($nombre_de_usuario);
                $this->_view->setParam('nombre_de_usuario', $nombre_de_usuario);
                $this->_view->setParam('pregunta_secreta', $pregunta_secreta);
                $this->_view->setParam('error', true);
                $this->_view->renderizar('responderPreguntaSecreta');

    }


}

?>
