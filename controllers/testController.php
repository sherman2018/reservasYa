<?php
include_once('models/testModel.php');

class TestController extends Controller
{
    private $testModel;
    public function __construct() {
        parent::__construct();
    }
    public function index(){}
    public function iniciar()
    {
        $testModel = new TestModel();
        $testModel->iniciar();
    }

public function consumirHabitacionView(){

    $this->_view->renderizar('consumirHabitacion');
}

public function mostrarOpcionesTesting(){
    $this->_view->renderizar('mostrarOpcionesTesting');
}
}
?>