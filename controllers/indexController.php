<?php
include_once('models/ciudadModel.php');
require_once(ROOT . DS. 'models'. DS.'vueloModel.php');
class indexController extends Controller
{
    private $vueloModel;
    public function __construct() {
        parent::__construct();
    }
    
    public function index()
    {
        $ciudadModel = new ciudadModel();
        $this->_view->setParam('ciudades', $ciudadModel->getCiudades());
        $this->_view->renderizar('index');
        $ciudadModel=null;
    }
    public function pagina2()
    {
        // Previo a renderizar seteo parametros
        $params = array();
        $this->_view->setParam('parametro1', "valor 1");
        $this->_view->setParam('parametro2', "valor 2");
        $this->_view->setParam('parametro3', "valor 3");
        // Ultima linea
        $this->_view->renderizar('pagina2');
    }
}
?>