<?php

require_once(ROOT . DS . 'models' . DS . 'vueloModel.php');
require_once(ROOT . DS . 'models' . DS . 'ciudadModel.php');
require_once(ROOT . DS . 'models' . DS . 'aerolineaModel.php');

class vueloController extends Controller {

    private $vueloModel;

    public function __construct() {
        parent::__construct();
        $this->vueloModel = new vueloModel;
        $this->ciudadModel = new ciudadModel;
        $this->aerolineaModel = new aerolineaModel;
    }

    public function index() {
        $this->autorizar('cliente');
        $vuelos = $this->vueloModel->getVuelos();

        $this->_view->setParam("vuelos", $vuelos);
        $this->_view->renderizar('index');
    }

    public function buscarVuelo() {
        
        $clase = null;
        $ciudad_origen = $_POST['origen'];
        $ciudad_destino = $_POST['destino'];
        $fechaVuelo = $_POST['fechaVuelo'];
        $cantidad = $_POST['cantidad'];
        if (isset($_POST['clase'])) {
            $clase = $_POST['clase'];
        }

        $cantidadEconomica = 0;
        $cantidadPrimera = 0;
        $cantidadBusiness = 0;
        if ($clase == 1) {
            $cantidadEconomica = $cantidad;
        } else {
            if ($clase == 2) {
                $cantidadBusiness = $cantidad;
            } else {
                if ($clase == 3) {
                    $cantidadPrimera = $cantidad;
                } else {

                    $cantidadEconomica = $cantidad;
                    $cantidadBusiness = $cantidad;
                    $cantidadPrimera = $cantidad;
                }
            }
        }

        $vuelos = $this->vueloModel->buscarVuelos($ciudad_origen, $ciudad_destino, $fechaVuelo, $cantidadEconomica, $cantidadPrimera, $cantidadBusiness);

        $tramoUno = $this->vueloModel->buscarVuelosConEscalaTramoUno($ciudad_origen, $fechaVuelo, $cantidadEconomica, $cantidadPrimera, $cantidadBusiness);

        $tramoDos = $this->vueloModel->buscarVuelosConEscalaTramoDos($ciudad_destino, $fechaVuelo, $cantidadEconomica, $cantidadPrimera, $cantidadBusiness);

        $_SESSION['primerFiltrado']['segundo_filtrado_activo'] = 1;
        $_SESSION['primerFiltrado']['ciudad_origen'] = $ciudad_origen;
        $_SESSION['primerFiltrado']['ciudad_destino'] = $ciudad_destino;
        $_SESSION['primerFiltrado']['fecha_vuelo'] = $fechaVuelo;
        $_SESSION['primerFiltrado']['cantidad_economica'] = $cantidadEconomica;
        $_SESSION['primerFiltrado']['cantidad_primera'] = $cantidadPrimera;
        $_SESSION['primerFiltrado']['cantidad_business'] = $cantidadBusiness;
        $_SESSION['primerFiltrado']['cantidad'] = $cantidad;
        $_SESSION['primerFiltrado']['clase'] = $clase;
        $segundo_filtrado_activo = $_SESSION['primerFiltrado']['segundo_filtrado_activo'];
        $_SESSION['primerFiltrado']['vuelos'] = $vuelos;
        $_SESSION['primerFiltrado']['tramoUno'] = $tramoUno;
        $_SESSION['primerFiltrado']['tramoDos'] = $tramoDos;

        $this->_view->setParam("segundo_filtrado_activo", $segundo_filtrado_activo);
        $this->_view->setParam("vuelos", $vuelos);
        $this->_view->setParam("tramoUno", $tramoUno);
        $this->_view->setParam("tramoDos", $tramoDos);

        if (isset($cantidad)) {
            $this->_view->setParam("cantidad", $cantidad);
        }
        $this->_view->setParam("clase", $clase);




        //$this->_view->setParam("factor_business",$factor_business);
        //$this->_view->setParam("factor_primera",$factor_primera);
        if (!empty($vuelos) || (!empty($tramoUno) && !empty($tramoDos))) {

            $this->_view->renderizar('index');
        } else {
            $this->_view->renderizar('sinVuelos');
        }
    }

    private function obtenerFecha($fecha, $hora) {
        $format = 'Y-m-d H:i';
        $datetime = DateTime::createFromFormat($format, $fecha . ' ' . $hora);
        return $datetime->format('Y-m-d H:i:s');
    }

    public function segundoFiltrado() {
        $clase = null;
        $segundo_filtrado_activo = $_SESSION['primerFiltrado']['segundo_filtrado_activo'] = 0;
        $ciudad_origen = $_SESSION['primerFiltrado']['ciudad_origen'];
        $ciudad_destino = $_SESSION['primerFiltrado']['ciudad_destino'];
        $fecha_vuelo = $_SESSION['primerFiltrado']['fecha_vuelo'];
        $cantidad_economica = $_SESSION['primerFiltrado']['cantidad_economica'];
        $cantidad_primera = $_SESSION['primerFiltrado']['cantidad_primera'];
        $cantidad_business = $_SESSION['primerFiltrado']['cantidad_business'];
        $cantidad = $_SESSION['primerFiltrado']['cantidad'];
        $clase = $_SESSION['primerFiltrado']['clase'];
        
        if(!isset($_SESSION['primerFiltrado']['clase'])){
            $clase = $this->getPostParam('clase');
        }
        if ($clase == 1) {
            $cantidadEconomica = $cantidad;
        } else {
            if ($clase == 2) {
                $cantidadBusiness = $cantidad;
            } else {
                if ($clase == 3) {
                    $cantidadPrimera = $cantidad;
                } else {

                    $cantidadEconomica = $cantidad;
                    $cantidadBusiness = $cantidad;
                    $cantidadPrimera = $cantidad;
                }
            }
        }
        //$aerolineaFiltrado = $this->getPostParam('aerolinea');
        if (isset($_POST['aerolinea'])){
            $aerolineaFiltrado = $_POST['aerolinea'];
        }
        
        $vuelos = $this->vueloModel->buscarVuelos($ciudad_origen, $ciudad_destino, $fecha_vuelo, $cantidad_economica, $cantidad_primera, $cantidad_business);

        $tramoUno = $this->vueloModel->buscarVuelosConEscalaTramoUno($ciudad_origen, $fecha_vuelo, $cantidad_economica, $cantidad_primera, $cantidad_business);

        $tramoDos = $this->vueloModel->buscarVuelosConEscalaTramoDos($ciudad_destino, $fecha_vuelo, $cantidad_economica, $cantidad_primera, $cantidad_business);

        $filterVuelos = [];
        $filterTramoUno = [];
        $filterTramoDos = [];
        
        if (isset($_POST['aerolinea'])){
            foreach ($vuelos as $key => $vuelo) {
                if ($vuelo['aerolinea'] == $aerolineaFiltrado) {
                    $filterVuelos[] = $vuelo;
                }
            }
        }else{
            $filterVuelos = $vuelos;
        }
        
        if (isset($_POST['aerolinea'])){
            foreach ($tramoUno as $key => $vuelo) {
                if ($vuelo['aerolinea'] == $aerolineaFiltrado) {
                    $filterTramoUno[] = $vuelo;
                }
            }
        }else{
            $filterTramoUno = $tramoUno;
        }

        if (isset($_POST['aerolinea'])){
            foreach ($tramoDos as $key => $vuelo) {
                if ($vuelo['aerolinea'] == $aerolineaFiltrado) {
                    $filterTramoDos[] = $vuelo;
                }
            }
        }else{
            $filterTramoDos = $tramoDos;
        }
        
        $this->_view->setParam("segundo_filtrado_activo", $segundo_filtrado_activo);
        $this->_view->setParam("vuelos", $filterVuelos);
        $this->_view->setParam("tramoUno", $filterTramoUno);
        $this->_view->setParam("tramoDos", $filterTramoDos);
        if (isset($cantidad)) {
            $this->_view->setParam("cantidad", $cantidad);
        }
        $this->_view->setParam("clase", $clase);
        if (!empty($filterVuelos) || (!empty($tramoUno) && !empty($tramoDos))) {
            $this->_view->renderizar('index');
        } else {
            $this->_view->renderizar('sinVuelos');
        }
    }
    
    public function quitarFiltrado(){
        $segundo_filtrado_activo = $_SESSION['primerFiltrado']['segundo_filtrado_activo'] = 1;
        $ciudad_origen = $_SESSION['primerFiltrado']['ciudad_origen'];
        $ciudad_destino = $_SESSION['primerFiltrado']['ciudad_destino'];
        $fecha_vuelo = $_SESSION['primerFiltrado']['fecha_vuelo'];
        $cantidad_economica = $_SESSION['primerFiltrado']['cantidad_economica'];
        $cantidad_primera = $_SESSION['primerFiltrado']['cantidad_primera'];
        $cantidad_business = $_SESSION['primerFiltrado']['cantidad_business'];
        $cantidad = $_SESSION['primerFiltrado']['cantidad'];
        $clase = $_SESSION['primerFiltrado']['clase'];
        $vuelos = $_SESSION['primerFiltrado']['vuelos'];
        $tramoUno = $_SESSION['primerFiltrado']['tramoUno'];
        $tramoDos = $_SESSION['primerFiltrado']['tramoDos'];
        
        $this->_view->setParam("clase", $clase);
        $this->_view->setParam("cantidad", $cantidad);        
        $this->_view->setParam("segundo_filtrado_activo", $segundo_filtrado_activo);
        $this->_view->setParam("vuelos", $vuelos);
        $this->_view->setParam("tramoUno", $tramoUno);
        $this->_view->setParam("tramoDos", $tramoDos);
        
        $this->_view->renderizar('index');
    }

    public function create() {
        $this->autorizar('comercial');

        $id_aerolinea = $_POST['aerolinea'];
        $fechaSalida = $_POST['fechaSalida'];
        $fechaLlegada = $_POST['fechaLlegada'];
        $id_origen = $_POST['origen'];
        $id_destino = $_POST['destino'];
        $cantEconomica = $_POST['cantEconomica'];
        $cantPrimera = $_POST['cantPrimera'];
        $cantBusiness = $_POST['cantBusiness'];
        $precio = $_POST['precio'];

        $ultimoVuelo = $this->obtenerFecha($_POST['fechaUltimoVuelo'], $_POST['horaSalida']);
        $fechaSalida = $this->obtenerFecha($_POST['fechaSalida'], $_POST['horaSalida']);
        $fechaLlegada = $this->obtenerFecha($_POST['fechaLlegada'], $_POST['horaLlegada']);



        $fechaSalida = new DateTime($fechaSalida);
        $fechaLlegada = new DateTime($fechaLlegada);
        $ultimoVuelo = new DateTime($ultimoVuelo);





        while ($fechaSalida->format('Y-m-d H:i:s') !== $ultimoVuelo->format('Y-m-d H:i:s')) {
            $this->vueloModel->crear($id_aerolinea, $fechaSalida->format('Y-m-d H:i:s'), $fechaLlegada->format('Y-m-d H:i:s'), $id_origen, $id_destino, $cantEconomica, $cantPrimera, $cantBusiness, $precio);
            $fechaSalida->modify('+1 day');
            $fechaLlegada->modify('+1 day');
        }
        $this->vueloModel->crear($id_aerolinea, $fechaSalida->format('Y-m-d H:i:s'), $fechaLlegada->format('Y-m-d H:i:s'), $id_origen, $id_destino, $cantEconomica, $cantPrimera, $cantBusiness, $precio);
        $this->_view->setParam("resultado", true);
        $this->createVueloView();
    }

    public function createVueloView() {
        $this->autorizar('comercial');
        //se deben crear los arrays ciudades y aerolineas para mandarlos por twig como parametros

        $ciudades = $this->ciudadModel->getCiudades();
        $this->_view->setParam("ciudades", $ciudades);
        $aerolineas = $this->aerolineaModel->getAerolineas();
        $this->_view->setParam("aerolineas", $aerolineas);
        $this->_view->renderizar('create-vuelo');
    }

    public function agregarCarrito() {
        $this->autorizar('cliente');
        $id_vuelo = $_POST['id_vuelo'];
        $precio_abonado = $_POST['precio_abonado'];
        $cantidad_primera = $_POST['cantidadad_primera'];
        $cantidad_economica = $_POST['cantidad_economica'];
        $cantidad_business = $_POST['cantidad_business'];
        $id_usuario = $_POST['id_usuario'];
        $estado = 'en carrito';
        $this->vueloModel->agregarCarrito($id_vuelo, $precio_abonado, $cantidad_primera, $cantidad_economica, $cantidad_business, $id_usuario, $estado);
                
        header('Location:../carrito');
    }

}

?>