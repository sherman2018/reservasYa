<?php
require_once(ROOT . DS. 'models'. DS.'usuarioModel.php');
class carritoController extends Controller
{
    
    public function __construct() {
        parent::__construct();
        $this->usuarioModel = new usuarioModel;
    }
    
    public function index()
    {
        $id_usuario = $_SESSION['id_usuario'];
        $vuelos = $this->usuarioModel->obtenerVuelos($id_usuario);
        $this->_view->setParam("reservasVuelos",$vuelos);
        $autos = $this->usuarioModel->obtenerAutos($id_usuario);
        $this->_view->setParam("reservasAutos",$autos);
        $habitaciones = $this->usuarioModel->obtenerHabitaciones($id_usuario);
        $this->_view->setParam("reservasHabitaciones",$habitaciones);
        $this->_view->renderizar('index');
        
    }

    public function quitarReservaVuelo(){
        $id_reserva_vuelo = $_GET['id_reserva_vuelo'];
        $id_vuelo = $_GET['id_vuelo'];
        $cantidad_economica = $_GET['cantidad_economica'];
        $cantidad_primera = $_GET['cantidad_primera'];
        $cantidad_business = $_GET['cantidad_business'];
        $this->usuarioModel->quitarReservaVuelo($id_reserva_vuelo,$id_vuelo,$cantidad_economica, $cantidad_primera, $cantidad_business);
        $this->index();
    }

    public function quitarReservaAuto(){
        $id_reserva_auto = $_GET['id_reserva_auto'];
        $this->usuarioModel->quitarReservaAuto($id_reserva_auto);
        $this->index();
    }

    public function quitarReservaHabitacion(){
        $id_reserva_habitacion = $_GET['id_reserva_habitacion'];
        $this->usuarioModel->quitarReservaHabitacion($id_reserva_habitacion);
        $this->index();
    }

}
?>